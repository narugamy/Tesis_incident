<?php
	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/

	Route::get('/', function () {
		return redirect()->route('admin.login');
	});
	Route::namespace('Admin')->prefix('admin')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest:admin')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware(['auth:admin'])->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('admin.logout');
			});
		});
		Route::middleware(['auth:admin'])->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('admin.index');
			});
			Route::namespace('User')->prefix('usuario')->group(function () {
				Route::get('/', 'UserController@index')->name('admin.user.index');
				Route::get('table', 'UserController@table')->name('admin.user.table');
				Route::get('create', 'UserController@create')->name('admin.user.create');
				Route::post('create', 'UserController@store');
				Route::get('{user}', 'UserController@show')->name('admin.user.update');
				Route::put('{user}', 'UserController@update');
				Route::get('{user}/delete', 'UserController@delete')->name('admin.user.delete');
				Route::get('{user}/destroy', 'UserController@destroy')->name('admin.user.destroy');
				Route::get('{user}/permission', 'UserController@show_permission')->name('admin.user.permission');
				Route::post('{user}/permission', 'UserController@store_permission');
			});
			Route::namespace('Module')->prefix('modulo')->group(function () {
				Route::get('/', 'ModuleController@index')->name('admin.module.index');
				Route::get('table', 'ModuleController@table')->name('admin.module.table');
				Route::get('create', 'ModuleController@create')->name('admin.module.create');
				Route::post('create', 'ModuleController@store');
				Route::get('{module}', 'ModuleController@show')->name('admin.module.update');
				Route::put('{module}', 'ModuleController@update');
				Route::get('{module}/delete', 'ModuleController@delete')->name('admin.module.delete');
				Route::get('{module}/destroy', 'ModuleController@destroy')->name('admin.module.destroy');
			});
			Route::namespace('Permission')->prefix('modulo/{modulo}/permiso')->group(function () {
				Route::get('/', 'PermissionController@index')->name('admin.permission.index');
				Route::get('table', 'PermissionController@table')->name('admin.permission.table');
				Route::get('create', 'PermissionController@create')->name('admin.permission.create');
				Route::post('create', 'PermissionController@store');
				Route::get('{permission}', 'PermissionController@show')->name('admin.permission.update');
				Route::put('{permission}', 'PermissionController@update');
				Route::get('{permission}/delete', 'PermissionController@delete')->name('admin.permission.delete');
				Route::get('{permission}/destroy', 'PermissionController@destroy')->name('admin.permission.destroy');
			});
			Route::namespace('Role')->prefix('rol')->group(function () {
				Route::get('/', 'RoleController@index')->name('admin.role.index');
				Route::get('table', 'RoleController@table')->name('admin.role.table');
				Route::get('create', 'RoleController@create')->name('admin.role.create');
				Route::post('create', 'RoleController@store');
				Route::get('{role}', 'RoleController@show')->name('admin.role.update');
				Route::put('{role}', 'RoleController@update');
				Route::get('{role}/delete', 'RoleController@delete')->name('admin.role.delete');
				Route::get('{role}/destroy', 'RoleController@destroy')->name('admin.role.destroy');
				Route::get('{role}/permission', 'RoleController@show_permission')->name('admin.role.permission');
				Route::post('{role}/permission', 'RoleController@store_permission');
			});
			Route::namespace('Company')->prefix('empresa')->group(function () {
				Route::get('/', 'CompanyController@index')->name('admin.company.index');
				Route::get('table', 'CompanyController@table')->name('admin.company.table');
				Route::get('create', 'CompanyController@create')->name('admin.company.create');
				Route::post('create', 'CompanyController@store');
				Route::get('{company}', 'CompanyController@show')->name('admin.company.update');
				Route::put('{company}', 'CompanyController@update');
				Route::get('{company}/delete', 'CompanyController@delete')->name('admin.company.delete');
				Route::get('{company}/destroy', 'CompanyController@destroy')->name('admin.company.destroy');
			});
			Route::namespace('Project')->prefix('empresa/{empresa}/proyecto')->group(function () {
				Route::get('/', 'ProjectController@index')->name('admin.project.index');
				Route::get('table', 'ProjectController@table')->name('admin.project.table');
				Route::get('create', 'ProjectController@create')->name('admin.project.create');
				Route::post('create', 'ProjectController@store');
				Route::get('{project}', 'ProjectController@show')->name('admin.project.update');
				Route::put('{project}', 'ProjectController@update');
				Route::get('{project}/delete', 'ProjectController@delete')->name('admin.project.delete');
				Route::get('{project}/destroy', 'ProjectController@destroy')->name('admin.project.destroy');
			});
			Route::namespace('Project')->prefix('empresa/{empresa}/proyecto')->group(function () {
				Route::get('/', 'ProjectController@index')->name('admin.project.index');
				Route::get('table', 'ProjectController@table')->name('admin.project.table');
				Route::get('create', 'ProjectController@create')->name('admin.project.create');
				Route::post('create', 'ProjectController@store');
				Route::get('{project}', 'ProjectController@show')->name('admin.project.update');
				Route::put('{project}', 'ProjectController@update');
				Route::get('{project}/delete', 'ProjectController@delete')->name('admin.project.delete');
				Route::get('{project}/destroy', 'ProjectController@destroy')->name('admin.project.destroy');
			});
			Route::namespace('Issue')->prefix('proyecto/{proyecto}/issue')->group(function () {
				Route::get('/', 'IssueController@index')->name('admin.issue.index');
				Route::get('table', 'IssueController@table')->name('admin.issue.table');
				Route::get('create', 'IssueController@create')->name('admin.issue.create');
				Route::post('create', 'IssueController@store');
				Route::get('{issue}', 'IssueController@show')->name('admin.issue.update');
				Route::put('{issue}', 'IssueController@update');
				Route::get('{issue}/delete', 'IssueController@delete')->name('admin.issue.delete');
				Route::get('{issue}/destroy', 'IssueController@destroy')->name('admin.issue.destroy');
			});
			Route::namespace('Comment')->prefix('issue/{issue}/comment')->group(function () {
				Route::get('/', 'CommentController@index')->name('admin.comment.index');
				Route::get('create', 'CommentController@create')->name('admin.comment.create');
				Route::post('create', 'CommentController@store');
				Route::get('{comment}', 'CommentController@show')->name('admin.comment.update');
				Route::put('{comment}', 'CommentController@update');
				Route::get('{comment}/delete', 'CommentController@delete')->name('admin.comment.delete');
				Route::get('{comment}/destroy', 'CommentController@destroy')->name('admin.comment.destroy');
			});
		});
	});
	Route::namespace('Developer')->prefix('developer')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest:developer')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('developer.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware(['auth:developer'])->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('developer.logout');
			});
		});
		Route::middleware(['auth:developer'])->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('developer.index');
			});
			Route::namespace('Company')->prefix('empresa')->group(function () {
				Route::get('/', 'CompanyController@index')->name('developer.company.index');
				Route::get('table', 'CompanyController@table')->name('developer.company.table');
				Route::get('create', 'CompanyController@create')->name('developer.company.create');
				Route::post('create', 'CompanyController@store');
				Route::get('{company}', 'CompanyController@show')->name('developer.company.update');
				Route::put('{company}', 'CompanyController@update');
				Route::get('{company}/delete', 'CompanyController@delete')->name('developer.company.delete');
				Route::get('{company}/destroy', 'CompanyController@destroy')->name('developer.company.destroy');
			});
			Route::namespace('Project')->prefix('proyecto')->group(function () {
				Route::get('/', 'ProjectController@index')->name('developer.project.index');
				Route::get('table', 'ProjectController@table')->name('developer.project.table');
				Route::get('create', 'ProjectController@create')->name('developer.project.create');
				Route::post('create', 'ProjectController@store');
				Route::get('{project}', 'ProjectController@show')->name('developer.project.update');
				Route::put('{project}', 'ProjectController@update');
				Route::get('{project}/delete', 'ProjectController@delete')->name('developer.project.delete');
				Route::get('{project}/destroy', 'ProjectController@destroy')->name('developer.project.destroy');
			});
			Route::namespace('Issue')->prefix('proyecto/{proyecto}/issue')->group(function () {
				Route::get('/', 'IssueController@index')->name('developer.issue.index');
				Route::get('table', 'IssueController@table')->name('developer.issue.table');
				Route::get('create', 'IssueController@create')->name('developer.issue.create');
				Route::post('create', 'IssueController@store');
				Route::get('{issue}', 'IssueController@show')->name('developer.issue.update');
				Route::get('{issue}/assign', 'IssueController@show_assign')->name('developer.issue.assign');
				Route::post('{issue}/assign', 'IssueController@assign');
				Route::put('{issue}', 'IssueController@update');
				Route::get('{issue}/delete', 'IssueController@delete')->name('developer.issue.delete');
				Route::get('{issue}/destroy', 'IssueController@destroy')->name('developer.issue.destroy');
			});
			Route::namespace('Assign')->prefix('assign_issue')->group(function () {
				Route::get('/', 'IssueController@index')->name('developer.assign.issue.index');
				Route::get('table', 'IssueController@table')->name('developer.assign.issue.table');
				Route::get('create', 'IssueController@create')->name('developer.assign.issue.create');
				Route::post('create', 'IssueController@store');
				Route::get('{issue}', 'IssueController@show')->name('developer.assign.issue.update');
				Route::put('{issue}', 'IssueController@update');
				Route::get('{issue}/close', 'IssueController@close')->name('developer.assign.issue.close');
				Route::get('{issue}/delete', 'IssueController@delete')->name('developer.issue.assign.delete');
				Route::get('{issue}/destroy', 'IssueController@destroy')->name('developer.issue.assign.destroy');
			});
			Route::namespace('Assign')->prefix('assign_issue/{issue}/comment')->group(function () {
				Route::get('/', 'CommentController@index')->name('developer.assign.comment.index');
				Route::get('table', 'CommentController@table')->name('developer.assign.comment.table');
				Route::get('create', 'CommentController@create')->name('developer.assign.comment.create');
				Route::post('create', 'CommentController@store');
				Route::get('{comment}', 'CommentController@show')->name('developer.assign.comment.update');
				Route::put('{comment}', 'CommentController@update');
				Route::get('{comment}/delete', 'CommentController@delete')->name('developer.assign.comment.delete');
				Route::get('{comment}/destroy', 'CommentController@destroy')->name('developer.assign.comment.destroy');
			});
			Route::namespace('My')->prefix('mis_issue')->group(function () {
				Route::get('/', 'IssueController@index')->name('developer.my.issue.index');
				Route::get('table', 'IssueController@table')->name('developer.my.issue.table');
				Route::get('create', 'IssueController@create')->name('developer.my.issue.create');
				Route::post('create', 'IssueController@store');
				Route::get('{issue}', 'IssueController@show')->name('developer.my.issue.update');
				Route::put('{issue}', 'IssueController@update');
				Route::get('{issue}/close', 'IssueController@close')->name('developer.my.issue.close');
				Route::get('{issue}/delete', 'IssueController@delete')->name('developer.my.issue.delete');
				Route::get('{issue}/destroy', 'IssueController@destroy')->name('developer.my.issue.destroy');
			});
			Route::namespace('My')->prefix('mis_issue/{issue}/comment')->group(function () {
				Route::get('/', 'CommentController@index')->name('developer.my.comment.index');
				Route::get('table', 'CommentController@table')->name('developer.my.comment.table');
				Route::get('create', 'CommentController@create')->name('developer.my.comment.create');
				Route::post('create', 'CommentController@store');
				Route::get('{comment}', 'CommentController@show')->name('developer.my.comment.update');
				Route::put('{comment}', 'CommentController@update');
				Route::get('{comment}/delete', 'CommentController@delete')->name('developer.my.comment.delete');
				Route::get('{comment}/destroy', 'CommentController@destroy')->name('developer.my.comment.destroy');
			});
		});
	});