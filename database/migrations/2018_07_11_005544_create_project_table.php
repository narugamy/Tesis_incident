<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateProjectTable extends Migration {

		public function up() {
			Schema::create('project', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('company_id')->unsigned()->index();
				$table->foreign('company_id')->references('id')->on('company')->onDelete('cascade');
				$table->string('name');
				$table->text('description');
				$table->string('slug', 191)->unique();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('project');
		}

	}
