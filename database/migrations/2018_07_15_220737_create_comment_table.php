<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateCommentTable extends Migration {

		public function up() {
			Schema::create('comment', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('issue_id')->unsigned()->index();
				$table->foreign('issue_id')->references('id')->on('issue')->onDelete('cascade');
				$table->integer('user_id')->unsigned()->index();
				$table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
				$table->integer('archive_id')->unsigned()->index()->nullable();
				$table->foreign('archive_id')->references('id')->on('archive')->onDelete('cascade');
				$table->text('description');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('comment');
		}

	}
