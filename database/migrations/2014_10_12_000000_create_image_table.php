<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateImageTable extends Migration {

		public function up() {
			Schema::create('image', function (Blueprint $table) {
				$table->increments('id');
				$table->string('name');
				$table->text('url');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('image');
		}

	}
