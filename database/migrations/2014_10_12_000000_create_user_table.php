<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateUserTable extends Migration {

		public function up() {
			Schema::create('user', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('company_id')->unsigned()->index()->nullable();
				$table->foreign('company_id')->references('id')->on('company')->onDelete('cascade');
				$table->integer('image_id')->unsigned()->index()->nullable();
				$table->foreign('image_id')->references('id')->on('image')->onDelete('cascade');
				$table->string('names');
				$table->string('surnames');
				$table->text('address')->nullable();
				$table->string('email', 191)->unique();
				$table->string('username', 191)->unique();
				$table->string('password');
				$table->rememberToken();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('user');
		}

	}
