<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateIssueTable extends Migration {

		public function up() {
			Schema::create('issue', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('project_id')->unsigned()->index();
				$table->foreign('project_id')->references('id')->on('project')->onDelete('cascade');
				$table->integer('client_id')->unsigned()->index()->nullable();
				$table->foreign('client_id')->references('id')->on('user')->onDelete('cascade');
				$table->integer('user_id')->unsigned()->index()->nullable();
				$table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
				$table->string('code', 12);
				$table->string('name', 191);
				$table->text('description');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('issue');
		}

	}
