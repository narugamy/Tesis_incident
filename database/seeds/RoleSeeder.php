<?php

	use App\Model\User;
	use Caffeinated\Shinobi\Models\Role;
	use Illuminate\Database\Seeder;
	use Illuminate\Support\Str;

	class RoleSeeder extends Seeder {

		public function run() {
			$rol = new Role();
			$rol->fill(['name' => 'Admin', 'slug' => Str::slug(strtolower('admin'))])->save();
			$admin = new User();
			$admin->fill(['names' => 'Admin', 'surnames' => 'Admin', 'email' => 'admin@gmail.com', 'username' => 'admin', 'password' => bcrypt('admin')])->save();
			$admin->assignRole($rol->id);
		}

	}
