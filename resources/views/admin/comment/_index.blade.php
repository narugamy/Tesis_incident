@extends('admin.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('admin.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('admin.company.index') }}">Compañias</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('admin.project.index', $issue->project->company->slug) }}">Proyectos</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('admin.issue.index', $issue->project->slug) }}">Issues</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Issues: {{ $issue->name }}</span>
				</li>
			</ul>
			@can('comment-create')
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a data-url="{{ route('admin.comment.create', $issue->id) }}" class="btn green btn-sm btn-outline btn-ajax"> Crear respuestas</a>
					</div>
				</div>
			@endcan
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		@if(session('success'))
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('success') }}
			</div>
		@elseif(session('improper'))
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('improper') }}
			</div>
		@endif
		<div class="portlet green box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user"></i>{{ $issue->client->full_name() }}
				</div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				{!! $issue->description !!}
			</div>
		</div>
		<div class="clearfix"></div>
		<br>
		@foreach($issue->comments as $comment)
			<div class="portlet {{ !$comment->deleted_at? 'green':'red' }} box">
				<div class="portlet-title">
					<div class="actions pull-left">
						<div class="btn-group">
							<a class="btn btn-circle red" href="javascript:;" data-toggle="dropdown">
								<i class="fa fa-user"></i> {{ $comment->user->full_name() }}
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu pull-left">
								@if(!$comment->deleted_at)
									@can('comment-update')
										<li>
											<a data-url="{{ route('admin.comment.update', [$comment->issue->id, $comment->id]) }}" class="btn-ajax"><i class="fa fa-pencil"></i> Mostrar</a>
										</li>
									@endcan
									@can('comment-delete')
										<li>
											<a data-url="{{ route('admin.comment.delete', [$comment->issue->id, $comment->id]) }}" data-message="Desea deshabilitar el comentario" class="btn-destroy"><i class="fa fa-low-vision"></i> Deshabilitar</a>
										</li>
									@endcan
									@can('comment-destroy')
										<li>
											<a data-url="{{ route('admin.comment.destroy', [$comment->issue->id, $comment->id]) }}" data-message="Desea eliminar el comentario" class="btn-destroy"><i class="fa fa-trash"></i> Eliminar</a>
										</li>
									@endcan
								@else
									@can('comment-delete')
										<li>
											<a data-url="{{ route('admin.comment.delete', [$comment->issue->id, $comment->id]) }}" data-message="Desea restaurar el comentario" class="btn-destroy"><i class="fa fa-recycle"></i> Restaurar</a>
										</li>
									@endcan
								@endif
							</ul>
						</div>
					</div>
				</div>
				<div class="portlet-body">
					{!! $comment->description !!}
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
		@endforeach
	</div>
	<div class="modal fade" id="ajax" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
@endsection