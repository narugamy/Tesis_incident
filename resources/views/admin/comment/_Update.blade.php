<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['admin.comment.update', $comment->issue->id, $comment->id], 'class' => 'form-horizontal form-file', 'method' => 'put']) }}
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps">
					<li>
						<a href="#tab1" data-toggle="tab" class="step">
							<span class="number"> 1 </span>
							<span class="desc">
								<i class="fa fa-check"></i> Principal
							</span>
						</a>
					</li>
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success active"></div>
				</div>
				<div class="tab-content portlet-body">
					<div class="tab-pane active" id="tab1">
						<div class="form-body">
							<div class="form-group form-md-line-input form-md-floating-label">
								<label for="description">Descripción</label>
								<textarea class="form-control edited" name="description" id="description" data-rule-required="true" data-rule-minlength="5" minlength="5">{{ $comment->description }}</textarea>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<label for="archive">Archivo</label>
								<input type="file" class="form-control edited" name="archive" id="archive">
							</div>
							<div class="form-group form-md-line-input form-md-floating-label text-center">
								<button class="btn green button-submit"> Actualizar
									<i class="fa fa-check"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
<script>
	$(document).ready(function () {
		if ($("#description").length > 0) {
			CKEDITOR.replace('description', {
				removePlugins: 'sourcearea,about',
			});
			CKEDITOR.editorConfig = function (config) {
				config.language = 'es';
				config.uiColor = '#F7B42C';
				config.toolbarCanCollapse = true;
			};
		}
	});
</script>