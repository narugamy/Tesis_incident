@extends('admin.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('admin.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('admin.company.index') }}">Compañias</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('admin.project.index', $project->company->slug) }}">Proyectos</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('admin.issue.index', $project->slug) }}">Issues</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Actualizar</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="form-wizzard">
			@if(session('error'))
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session('error') }}
				</div>
			@endif
			@if(count($errors) > 0)
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="portlet light bordered wizzard portlet-form">
				<div class="portlet-body form">
					{{ Form::open(['route' => ['admin.issue.update', $project->slug, $project->issue->id], 'class' => 'form-horizontal form-submit', 'method' => 'put']) }}
					<div class="form-wizard">
						<div class="form-body">
							<ul class="nav nav-pills nav-justified steps">
								<li>
									<a href="#tab1" data-toggle="tab" class="step">
										<span class="number"> 1 </span>
										<span class="desc">
								<i class="fa fa-check"></i> Principal
							</span>
									</a>
								</li>
							</ul>
							<div id="bar" class="progress progress-striped" role="progressbar">
								<div class="progress-bar progress-bar-success active"></div>
							</div>
							<div class="tab-content portlet-body">
								<div class="tab-pane active" id="tab1">
									<div class="form-body">
										<div class="form-group form-md-line-input form-md-floating-label">
											<input type="text" class="form-control edited" name="name" id="name" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" value="{{ $project->issue->name }}">
											<label for="name">Nombre</label>
										</div>
										<div class="form-group form-md-line-input form-md-floating-label">
											<label for="description">Descripción</label>
											<textarea class="form-control edited" name="description" id="description" data-rule-required="true" data-rule-minlength="5" minlength="5">{{ $project->issue->description }}</textarea>
										</div>
										<div class="clearfix"></div>
										<div class="form-group form-md-line-input form-md-floating-label text-center">
											<button class="btn green button-submit"> Actualizar
												<i class="fa fa-check"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
	<script>
		$(document).ready(function () {
			if ($("#description").length > 0) {
				CKEDITOR.replace('description', {
					removePlugins: 'sourcearea,about',
				});
				CKEDITOR.editorConfig = function (config) {
					config.language = 'es';
					config.height = 500;
				};
			}
		});
	</script>
@endsection