@extends('admin.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<span>Home</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}</h1>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat2 ">
					<div class="display">
						<div class="number">
							<h3 class="font-green-sharp">
								<span data-counter="counterup" data-value="7800">0</span>
							</h3>
							<small>TOTAL PROFIT</small>
						</div>
						<div class="icon">
							<i class="icon-pie-chart"></i>
						</div>
					</div>
					<div class="progress-info">
						<div class="progress">
							<span style="width: 76%;" class="progress-bar progress-bar-success green-sharp"></span>
						</div>
						<div class="status">
							<div class="status-title"> progress</div>
							<div class="status-number"> 76%</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat2 ">
					<div class="display">
						<div class="number">
							<h3 class="font-red-haze">
								<span data-counter="counterup" data-value="1349">0</span>
							</h3>
							<small>NEW FEEDBACKS</small>
						</div>
						<div class="icon">
							<i class="icon-like"></i>
						</div>
					</div>
					<div class="progress-info">
						<div class="progress">
							<span style="width: 85%;" class="progress-bar progress-bar-success red-haze"></span>
						</div>
						<div class="status">
							<div class="status-title"> change</div>
							<div class="status-number"> 85%</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat2 ">
					<div class="display">
						<div class="number">
							<h3 class="font-blue-sharp">
								<span data-counter="counterup" data-value="567">0</span>
							</h3>
							<small>NEW ORDERS</small>
						</div>
						<div class="icon">
							<i class="icon-basket"></i>
						</div>
					</div>
					<div class="progress-info">
						<div class="progress">
							<span style="width: 45%;" class="progress-bar progress-bar-success blue-sharp"></span>
						</div>
						<div class="status">
							<div class="status-title"> grow</div>
							<div class="status-number"> 45%</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat2 ">
					<div class="display">
						<div class="number">
							<h3 class="font-purple-soft">
								<span data-counter="counterup" data-value="276">0</span>
							</h3>
							<small>NEW USERS</small>
						</div>
						<div class="icon">
							<i class="icon-user"></i>
						</div>
					</div>
					<div class="progress-info">
						<div class="progress">
							<span style="width: 57%;" class="progress-bar progress-bar-success purple-soft"></span>
						</div>
						<div class="status">
							<div class="status-title"> change</div>
							<div class="status-number"> 57%</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection