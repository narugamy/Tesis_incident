<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['admin.role.permission', $role->slug], 'class' => 'form-horizontal form-submit']) }}
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps nav-grid">
					@foreach($modules as $module)
						@if(count($module->permissions) > 0)
							<li>
								<a href="#tab{{ $loop->iteration }}" data-toggle="tab" class="step">
									<span class="number"> {{ $loop->iteration }} </span>
									<span class="desc"><i class="fa fa-check"></i> {{ $module->name }}</span>
								</a>
							</li>
						@endif
					@endforeach
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success active"></div>
				</div>
				<div class="tab-content portlet-body">
					@foreach($modules as $module)
						@if(count($module->permissions) > 0)
							<div class="tab-pane active" id="tab{{ $loop->iteration }}">
								<div class="form-body">
									<div class="form-group form-md-line-input form-md-floating-label text-center">
										<div class="md-checkbox-grid">
											@foreach($module->permissions as $permission)
												<div class="md-checkbox">
													<input type="checkbox" id="permission_{{$permission->id}}" class="md-check" name="permission[]" value="{{(int)$permission->id}}"
														{{ ($role->can($permission->slug))? "checked":'' }}>
													<label for="permission_{{$permission->id}}">
														<span></span>
														<span class="check"></span>
														<span class="box"></span> {{ $permission->name }} </label>
												</div>
											@endforeach
										</div>
									</div>
									<div class="form-group form-md-line-input form-md-floating-label text-center">
										<button class="btn green button-submit"> Guardar
											<i class="fa fa-check"></i>
										</button>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
					@endforeach
					<ul class="pager wizard">
						<li class="previous">
							<a class="btn default button-previous"><i class="fa fa-angle-left"></i>Anterior</a>
						</li>
						<li class="next">
							<a class="btn btn-outline green button-next">Siguiente <i class="fa fa-angle-right"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>