<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['developer.project.create'], 'class' => 'form-horizontal form-modal']) }}
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps">
					<li>
						<a href="#tab1" data-toggle="tab" class="step">
							<span class="number"> 1 </span>
							<span class="desc">
								<i class="fa fa-check"></i> Principal
							</span>
						</a>
					</li>
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success active"></div>
				</div>
				<div class="tab-content portlet-body">
					<div class="tab-pane active" id="tab1">
						<div class="form-body">
							<div class="form-group form-md-line-input form-md-floating-label">
								{{ Form::select('company_id', $companies, null, ['placeholder' => 'Seleccione una', 'class' => 'form-control edited', 'id' => 'company_id']) }}
								<label for="company_id">Compañia</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control" name="name" id="name" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191">
								<label for="name">Nombre</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<label for="description">Descripción</label>
								<textarea class="form-control edited" name="description" id="description" data-rule-required="true" data-rule-minlength="5" minlength="5"></textarea>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label text-center">
								<button class="btn green button-submit"> Crear
									<i class="fa fa-check"></i>
								</button>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
<script>
	$(document).ready(function () {
		if ($("#description").length > 0) {
			CKEDITOR.replace('description', {
				removePlugins: 'sourcearea,about',
			});
			CKEDITOR.editorConfig = function (config) {
				config.language = 'es';
				config.height = 500;
			};
		}
	});
</script>