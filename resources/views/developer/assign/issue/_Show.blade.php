<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['developer.issue.assign', $issue->project->slug, $issue->id], 'class' => 'form-horizontal form-submit']) }}
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps">
					<li>
						<a href="#tab1" data-toggle="tab" class="step">
							<span class="number"> 1 </span>
							<span class="desc">
								<i class="fa fa-check"></i> Principal
							</span>
						</a>
					</li>
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success active"></div>
				</div>
				<div class="tab-content portlet-body">
					<div class="tab-pane active" id="tab1">
						<div class="form-body">
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control edited" id="name" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" value="{{ $issue->name }}" disabled>
								<label for="name">Nombre</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<label for="description">Descripción</label>
								<div>{!! $issue->description !!}</div>
							</div>
							<div class="clearfix"></div>
							<div class="form-group form-md-line-input form-md-floating-label text-center">
								<button type="button" class="btn white" data-dismiss="modal"> Cancelar
									<i class="fa fa-close"></i>
								</button>
								<button class="btn green button-submit"> Asignarme
									<i class="fa fa-check"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>