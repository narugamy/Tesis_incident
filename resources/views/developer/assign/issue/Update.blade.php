@extends('developer.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('developer.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('developer.assign.issue.index') }}">Mis Issues asignados</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('developer.assign.comment.index', $issue->id) }}">Comentarios del Issue: {{ $issue->name }}</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Actualizar</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="form-wizzard">
			@if(session('error'))
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session('error') }}
				</div>
			@endif
			@if(count($errors) > 0)
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			@include('developer.issue._Update')
		</div>
	</div>
@endsection
@section('scripts')
	<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
	<script>
		$(document).ready(function () {
			if ($("#description").length > 0) {
				CKEDITOR.replace('description', {
					removePlugins: 'sourcearea,about',
				});
				CKEDITOR.editorConfig = function (config) {
					config.language = 'es';
					config.height = 500;
				};
			}
		});
	</script>
@endsection