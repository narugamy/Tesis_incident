<?php

	namespace App\Http\Middleware;

	use Closure;
	use Illuminate\Support\Facades\Auth;

	class RedirectIfAuthenticated {

		public function handle($request, Closure $next, $guard = null) {
			if (Auth::guard($guard)->check()) :
				switch ($guard):
					case 'admin':
						$login = 'admin.index';
						break;
					case 'developer':
						$login = 'developer.index';
						break;
				endswitch;
				return redirect(route($login));
			endif;
			return $next($request);
		}
	}
