<?php

	namespace App\Http\Requests\Admin\Issue;

	use App\Model\Issue;
	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize() {
			$issue = Issue::find($this->segment(5));
			if($issue):
				return true;
			endif;
			return false;
		}

		public function rules() {
			$issue = Issue::find($this->segment(5));
			if($issue):
				return [
					'name' => 'required|min:3|max:21|unique:issue,name,'.$issue->id,
					'description' => 'required|min:5'
				];
			endif;
			return [];
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
