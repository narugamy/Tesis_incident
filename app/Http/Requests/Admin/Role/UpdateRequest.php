<?php

	namespace App\Http\Requests\Admin\Role;

	use Caffeinated\Shinobi\Models\Role;
	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize() {
			$role = Role::where(['slug' => $this->segment(3)])->first();
			if($role):
				return true;
			endif;
			return false;
		}

		public function rules() {
			$role = Role::where(['slug' => $this->segment(3)])->first();
			if($role):
				return [
					'name' => 'required|min:3|max:21|unique:role,name,'.$role->id,
					'description' => 'required|min:5'
				];
			endif;
			return [];
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
