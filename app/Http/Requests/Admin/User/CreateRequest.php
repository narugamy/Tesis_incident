<?php

	namespace App\Http\Requests\Admin\User;

	use Illuminate\Foundation\Http\FormRequest;

	class CreateRequest extends FormRequest {

		public function authorize () {
			return true;
		}

		public function rules () {
			return [
				'role_id' => 'required|integer|exists:role,id',
				'names' => 'required|min:3|max:191',
				'surnames' => 'required|min:3|max:191',
				'address' => 'nullable|min:4',
				'email' => 'required|min:5|max:191|email|unique:user',
				'username' => 'required|min:5|max:191|unique:user',
				'password' => 'required|min:5|max:191',
			];
		}

		public function messages(){
			return [
				'role_id.required'=>'requerido',
				'role_id.integer'=>'solo numero',
				'role_id.exists'=>'el ubigeo no existe',
				'names.required'=>'requerido',
				'names.min'=>'min. :min caracteres',
				'names.max'=>'max. :max caracteres',
				'surnames.required'=>'requerido',
				'surnames.min'=>'min. :min caracteres',
				'surnames.max'=>'max. :max caracteres',
				'address.min'=>'min. :min caracteres',
				'email.required'=>'requerido',
				'email.min'=>'min. :min caracteres',
				'email.max'=>'max. :max caracteres',
				'email.email'=>'solo correos',
				'email.unique'=>'ya existe',
				'username.required'=>'requerido',
				'username.min'=>'min. :min caracteres',
				'username.max'=>'max. :max caracteres',
				'username.unique'=>'ya existe',
				'password.required'=>'requerido',
				'password.min'=>'min. :min caracteres',
				'password.max'=>'max. :max caracteres',
			];
		}

	}
