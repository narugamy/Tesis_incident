<?php

	namespace App\Http\Requests\Admin\User;

	use App\Model\User;
	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize () {
			$user = User::where(['username' => $this->segment(3)])->first();
			if($user):
				return true;
			else:
				return false;
			endif;
		}

		public function rules () {
			$user = User::where(['username' => $this->segment(3)])->first();
			if($user):
				return [
					'role_id' => 'required|integer|exists:role,id',
					'names' => 'required|min:3|max:191',
					'surnames' => 'required|min:3|max:191',
					'address' => 'nullable|min:4',
					'email' => 'required|min:5|max:191|email|unique:user,email,'.$user->id,
					'username' => 'required|min:5|max:191|unique:user,username,'.$user->id,
					'password' => 'nullable|min:5|max:191',
				];
			else:
				return [];
			endif;
		}

		public function messages(){
			return [
				'role_id.required'=>'requerido',
				'role_id.integer'=>'solo numero',
				'role_id.exists'=>'el ubigeo no existe',
				'names.required'=>'requerido',
				'names.min'=>'min. :min caracteres',
				'names.max'=>'max. :max caracteres',
				'surnames.required'=>'requerido',
				'surnames.min'=>'min. :min caracteres',
				'surnames.max'=>'max. :max caracteres',
				'address.min'=>'min. :min caracteres',
				'email.required'=>'requerido',
				'email.min'=>'min. :min caracteres',
				'email.max'=>'max. :max caracteres',
				'email.email'=>'solo correos',
				'email.unique'=>'ya existe',
				'username.required'=>'requerido',
				'username.min'=>'min. :min caracteres',
				'username.max'=>'max. :max caracteres',
				'username.unique'=>'ya existe',
				'password.min'=>'min. :min caracteres',
				'password.max'=>'max. :max caracteres',
			];
		}

	}
