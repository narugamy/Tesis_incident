<?php

	namespace App\Http\Requests\Admin\Permission;

	use Illuminate\Foundation\Http\FormRequest;

	class CreateRequest extends FormRequest {

		public function authorize() {
			return true;
		}

		public function rules() {
			return [
				'name' => 'required|min:3|max:191',
				'route' => 'required|min:3|max:191|unique:permission',
				'description' => 'required|min:5'
			];
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
				'route.required'=>'requerido',
				'route.unique'=>'el usuario no existe',
				'route.min'=>'min. :min caracteres',
				'route.max'=>'max. :max caracteres',
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
