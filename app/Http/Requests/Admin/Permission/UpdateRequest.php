<?php

	namespace App\Http\Requests\Admin\Permission;

	use Caffeinated\Shinobi\Models\Permission;
	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize() {
			$permission = Permission::where(['slug' => $this->segment(5)])->first();
			if($permission):
				return true;
			endif;
			return false;
		}

		public function rules() {
			$permission = Permission::where(['slug' => $this->segment(5)])->first();
			if($permission):
				return [
					'name' => 'required|min:3|max:191',
					'route' => 'required|min:3|max:191|unique:permission,route,'.$permission->id,
					'description' => 'required|min:5'
				];
			endif;
			return [];
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
				'route.required'=>'requerido',
				'route.unique'=>'el usuario no existe',
				'route.min'=>'min. :min caracteres',
				'route.max'=>'max. :max caracteres',
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
