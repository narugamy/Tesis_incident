<?php

	namespace App\Http\Requests\Admin\Project;

	use App\Model\Project;
	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize() {
			$project = Project::where(['slug' => $this->segment(5)])->first();
			if($project):
				return true;
			endif;
			return false;
		}

		public function rules() {
			$project = Project::where(['slug' => $this->segment(5)])->first();
			if($project):
				return [
					'name' => 'required|min:3|max:21|unique:project,name,'.$project->id,
					'description' => 'required|min:5'
				];
			endif;
			return [];
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
