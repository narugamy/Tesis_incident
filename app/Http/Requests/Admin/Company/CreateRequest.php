<?php

	namespace App\Http\Requests\Admin\Company;

	use Illuminate\Foundation\Http\FormRequest;

	class CreateRequest extends FormRequest {

		public function authorize() {
			return true;
		}

		public function rules() {
			return [
				'name' => 'required|min:3|max:191|unique:company',
			];
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
			];
		}

	}
