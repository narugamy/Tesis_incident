<?php

	namespace App\Http\Requests\Admin\Company;

	use App\Model\Company;
	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize() {
			$company = Company::where(['slug' => $this->segment(3)])->first();
			if($company):
				return true;
			else:
				return false;
			endif;
		}

		public function rules() {
			$company = Company::where(['slug' => $this->segment(3)])->first();
			if($company):
				return [
					'name' => 'required|min:3|max:191|unique:company,name,'.$company->id,
				];
			else:
				return [];
			endif;
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
			];
		}

	}
