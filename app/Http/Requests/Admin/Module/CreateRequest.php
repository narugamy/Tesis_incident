<?php

	namespace App\Http\Requests\Admin\Module;

	use Illuminate\Foundation\Http\FormRequest;

	class CreateRequest extends FormRequest {

		public function authorize() {
			return true;
		}

		public function rules() {
			return [
				'name' => 'required|min:3|max:191|unique:module',
			];
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.unique'=>'el nombre ya existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
			];
		}

	}
