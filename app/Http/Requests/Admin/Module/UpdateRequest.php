<?php

	namespace App\Http\Requests\Admin\Module;

	use App\Model\Module;
	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize() {
			$module = Module::where(['slug' => $this->segment(3)])->first();
			if($module):
				return true;
			else:
				return false;
			endif;
		}

		public function rules() {
			$module = Module::where(['slug' => $this->segment(3)])->first();
			if($module):
				return [
					'name' => 'required|min:3|max:191|unique:module,name,'.$module->id,
				];
			else:
				return [];
			endif;
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
			];
		}

	}
