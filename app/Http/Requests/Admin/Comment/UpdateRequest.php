<?php

	namespace App\Http\Requests\Admin\Comment;

	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize() {
			return true;
		}

		public function rules() {
			return [
				'description' => 'required|min:5'
			];
		}

		public function messages(){
			return [
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
