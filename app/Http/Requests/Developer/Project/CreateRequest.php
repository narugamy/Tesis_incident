<?php

	namespace App\Http\Requests\Developer\Project;

	use Illuminate\Foundation\Http\FormRequest;

	class CreateRequest extends FormRequest {

		public function authorize() {
			return true;
		}

		public function rules() {
			return [
				'company_id' => 'required|exists:company,id',
				'name' => 'required|min:3|max:191|unique:project',
				'description' => 'required|min:5'
			];
		}

		public function messages(){
			return [
				'company_id.required'=>'requerido',
				'company_id.exists'=>'la compañia no existe',
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
