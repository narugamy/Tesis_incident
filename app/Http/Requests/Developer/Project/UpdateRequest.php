<?php

	namespace App\Http\Requests\Developer\Project;

	use App\Model\Project;
	use Illuminate\Foundation\Http\FormRequest;

	class UpdateRequest extends FormRequest {

		public function authorize() {
			$project = Project::where(['slug' => $this->segment(3)])->first();
			if($project):
				return true;
			endif;
			return false;
		}

		public function rules() {
			$project = Project::where(['slug' => $this->segment(3)])->first();
			if($project):
				return [
					'company_id' => 'required|exists:company,id',
					'name' => 'required|min:3|max:191|unique:project,name,'.$project->id,
					'description' => 'required|min:5'
				];
			endif;
			return [];
		}

		public function messages(){
			return [
				'company_id.required'=>'requerido',
				'company_id.exists'=>'la compañia no existe',
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
