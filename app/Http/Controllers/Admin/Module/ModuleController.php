<?php

	namespace App\Http\Controllers\Admin\Module;

	use App\Http\Requests\Admin\Module\CreateRequest;
	use App\Http\Requests\Admin\Module\UpdateRequest;
	use App\Model\Module;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Str;
	use Yajra\DataTables\Facades\DataTables;

	class ModuleController extends Controller {

		public function index() {
			return view('admin.module._index')->with(['title' => 'Panel de modulos', 'class_header' => 'page-container-bg-solid']);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				$auth = Auth::guard('admin')->user();
				try {
					return DataTables::of(Module::withTrashed())->addColumn('link', function ($row) use ($auth) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= ($auth->can('permission-index')) ? "<li><a href='" . route('admin.permission.index', $row->slug) . "'><i class='fa fa-list-alt'></i> Permisos</a></li>" : "";
							$buttons .= ($auth->can('module-update')) ? "<li><a data-url='" . route('admin.module.update', $row->slug) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
							$buttons .= ($auth->can('module-delete')) ? "<li><a data-url='" . route('admin.module.delete', $row->slug) . "' data-message='Desea deshabilitar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>" : "";
							$buttons .= ($auth->can('module-destroy')) ? "<li><a data-url='" . route('admin.module.destroy', $row->slug) . "' data-message='Desea eliminar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>" : "";
						else:
							$buttons .= ($auth->can('module-delete')) ? "<li><a data-url='" . route('admin.module.delete', $row->slug) . "' data-message='Desea restaurar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Deshabilitar</a></li>" : "";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addIndexColumn()->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => [], "error" => "Some sort of error occurred..."]);
				}
			else:
				return redirect()->route('admin.login');
			endif;
		}

		public function create(Request $request) {
			if ($request->ajax()):
				$data = view('admin.module._Create');
			else:
				$data = view('admin.module.Create')->with(['title' => 'Registro de modulo']);
			endif;
			return $data;
		}

		public function store(CreateRequest $request) {
			try {
				DB::beginTransaction();
				$module = new Module($request->all());
				$module->slug = Str::slug($module->name);
				$module->save();
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.module.index')], 'status' => 200, 'route' => route('admin.module.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.module.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $slug) {
			$module = Module::where(['slug' => $slug])->first();
			if ($module):
				if ($request->ajax()):
					$data = view('admin.module._Update');
				else:
					$data = view('admin.module.Update')->with(['title' => "Actualización del modulo: $module->name"]);
				endif;
				$data->with(['module' => $module]);
			else:
				if ($request->ajax()):
					$data = "Modulo no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el modulo no existe");
					$data = redirect(route('admin.module.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $slug) {
			$module = Module::where(['slug' => $slug])->first();
			if ($module):
				try {
					DB::beginTransaction();
					$module->fill($request->all())->save();
					DB::commit();
					$message = "Actualización exitosa";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.module.index')], 'status' => 200, 'route' => route('admin.module.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.module.update', $module->name), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el modulo no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.module.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $slug) {
			if ($request->ajax()):
				$module = Module::withTrashed()->where(['slug' => $slug])->first();
				if ($module):
					try {
						DB::beginTransaction();
						if ($module->deleted_at):
							$module->restore();
							$message = "Restauración exitosa";
						else:
							$module->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.module.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el modulo no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.module.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $slug) {
			if ($request->ajax()):
				$module = Module::withTrashed()->where(['slug' => $slug])->first();
				if ($module):
					try {
						DB::beginTransaction();
						$module->forceDelete();
						DB::commit();
						$message = "Eliminación exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.module.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el modulo no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.module.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
