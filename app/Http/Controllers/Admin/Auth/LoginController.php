<?php

	namespace App\Http\Controllers\Admin\Auth;

	use App\Http\Controllers\Controller;
	use App\Http\Requests\Admin\Login\LoginRequest;
	use App\Model\User;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Hash;

	class LoginController extends Controller {

		public function showLoginForm() {
			return view('admin.auth.app');
		}

		public function login(LoginRequest $request) {
			$user = User::where(['username' => $request->username])->first();
			if ($user):
				if (Hash::check($request->password, $user->password)):
					if ($user->isAdmin()):
						Auth::guard('admin')->login($user);
						$message = 'Registro exitoso';
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.index'), 'errors' => null], 'status' => 200, 'route' => route('admin.index'), 'message' => $message, 'type' => 'success'];
						return $this->optimize($array);
					else:
						$message = 'No tienes el rol correcto';
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['username' => 'Su cuenta no tiene el privilegio']], 'status' => 422, 'route' => route('admin.login'), 'message' => $message, 'type' => 'improper'];
						return $this->optimize($array);
					endif;
				else:
					$message = 'Contraseña incorrecta';
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['password' => 'Su contraseña es incorrecta']], 'status' => 422, 'route' => route('admin.login'), 'message' => $message, 'type' => 'improper'];
					return $this->optimize($array);
				endif;
			else:
				$message = 'El usuario no existe';
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['username' => 'Su cuenta no existe']], 'status' => 422, 'route' => route('admin.login'), 'message' => $message, 'type' => 'improper'];
				return $this->optimize($array);
			endif;
		}

		public function destroy(Request $request) {
			Auth::guard('admin')->logout();
			$message = 'Session cerrada exitosamente';
			$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'errors' => null], 'status' => 200, 'route' => route('admin.login'), 'message' => $message, 'type' => 'success'];
			return $this->optimize($array);
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
