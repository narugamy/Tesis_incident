<?php

	namespace App\Http\Controllers\Admin\Permission;

	use App\Http\Requests\Admin\Permission\CreateRequest;
	use App\Http\Requests\Admin\Permission\UpdateRequest;
	use App\Model\Module;
	use Caffeinated\Shinobi\Models\Permission;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class PermissionController extends Controller {

		public function index($slug) {
			$this->autorizate('index');
			$module = Module::where(['slug' => $slug])->with(['permissions' => function ($query) {
				return $query->withTrashed();
			}])->first();
			if ($module):
				$data = view('admin.permission._index')->with(['title' => "Panel de permisos del modulo: $module->name", 'class_header' => 'page-container-bg-solid', 'module' => $module]);
			else:
				$data = redirect(route('admin.module.index'));
			endif;
			return $data;
		}

		public function create(Request $request, $slug) {
			$this->autorizate('create');
			$module = Module::where(['slug' => $slug])->with('permissions')->first();
			if ($module):
				if ($request->ajax()):
					$data = view('admin.permission._Create');
				else:
					$data = view('admin.permission.Create')->with(['title' => 'Registro de permiso']);
				endif;
				$data->with(['module' => $module]);
			else:
				$data = redirect(route('admin.module.index'));
			endif;
			return $data;
		}

		public function table(Request $request, $slug) {
			if ($request->ajax()):
				$module = Module::where(['slug' => $slug])->with('permissions')->first();
				if ($module):
					$auth = Auth::guard('admin')->user();
					try {
						return DataTables::of(Permission::where(['module_id' => $module->id])->withTrashed())->addColumn('link', function ($row) use ($auth, $module) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
								$buttons .= ($auth->can('permission-update')) ? "<li><a data-url='" . route('admin.permission.update', [$module->slug, $row->slug]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
								$buttons .= ($auth->can('permission-delete')) ? "<li><a data-url='" . route('admin.permission.delete', [$module->slug, $row->slug]) . "' data-message='Desea deshabilitar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>" : "";
								$buttons .= ($auth->can('permission-destroy')) ? "<li><a data-url='" . route('admin.permission.destroy', [$module->slug, $row->slug]) . "' data-message='Desea eliminar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>" : "";
							else:
								$buttons .= ($auth->can('permission-delete')) ? "<li><a data-url='" . route('admin.permission.delete', [$module->slug, $row->slug]) . "' data-message='Desea restaurar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Deshabilitar</a></li>" : "";
							endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->addIndexColumn()->rawColumns(['link'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				return redirect()->route('admin.login');
			endif;
		}

		public function store(CreateRequest $request, $slug) {
			$this->autorizate('create');
			$module = Module::where(['slug' => $slug])->first();
			if ($module):
				try {
					DB::beginTransaction();
					$permission = new Permission($request->all());
					$permission->slug = str_replace('.', '-', strtolower($permission->route));
					$permission->module_id = $module->id;
					$permission->save();
					DB::commit();
					$message = "Registro exitoso";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.permission.index', $module->slug)], 'status' => 200, 'route' => route('admin.permission.index', $module->slug), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.permission.create', $module->slug), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "El modulo no existé";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.module.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $module_slug, $permission_slug) {
			$this->autorizate('update');
			$module = Module::where(['slug' => $module_slug])->first();
			if ($module):
				$permission = Permission::where(['slug' => $permission_slug, 'module_id' => $module->id])->first();
				if ($permission):
					if ($request->ajax()):
						$data = view('admin.permission._Update');
					else:
						$data = view('admin.permission.Update')->with(['title' => "Actualización del permiso: $permission->name"]);
					endif;
					$data->with(['permission' => $permission, 'module' => $module]);
				else:
					if ($request->ajax()):
						$data = "Permiso no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, el permiso no existe");
						$data = redirect(route('admin.permission.index'));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Modulo no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el modulo no existe");
					$data = redirect(route('admin.module.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $module_slug, $permission_slug) {
			$this->autorizate('update');
			$module = Module::where(['slug' => $module_slug])->first();
			if ($module):
				$permission = Permission::where(['slug' => $permission_slug, 'module_id' => $module->id])->first();
				if ($permission):
					try {
						DB::beginTransaction();
						$permission->fill($request->all())->save();
						DB::commit();
						$message = "Actualización exitosa";
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.permission.index', $module->slug)], 'status' => 200, 'route' => route('admin.permission.index', $module->slug), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.permission.update', $module->slug, $permission->slug), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					}
				else:
					$message = "No intentes algo indebido, el permiso no existe";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.permission.index', $module->slug), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = "No intentes algo indebido, el module no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.module.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $module_slug, $permission_slug) {
			$this->autorizate('delete');
			if ($request->ajax()):
				$module = Module::where(['slug' => $module_slug])->first();
				if ($module):
					$permission = Permission::withTrashed()->where(['slug' => $permission_slug, 'module_id' => $module->id])->first();
					if ($permission):
						try {
							DB::beginTransaction();
							if ($permission->deleted_at):
								$permission->restore();
								$message = "Restauración exitosa";
							else:
								$permission->delete();
								$message = "Deshabilitación exitosa";
							endif;
							DB::commit();
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.permission.index', $module->slug)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el permiso no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el rol no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.permission.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $module_slug, $permission_slug) {
			$this->autorizate('destroy');
			if ($request->ajax()):
				$module = Module::where(['slug' => $module_slug])->first();
				if ($module):
					$permission = Permission::withTrashed()->where(['slug' => $permission_slug, 'module_id' => $module->id])->first();
					if ($permission):
						try {
							DB::beginTransaction();
							$permission->forceDelete();
							DB::commit();
							$message = "Eliminación exitosa";
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.permission.index', $module->slug)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el permiso no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el modulo no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.permission.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

		private function autorizate($function) {
			//$this->authorize($function, Permission::class);
		}

	}
