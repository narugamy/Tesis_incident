<?php

	namespace App\Http\Controllers\Admin\Role;

	use App\Http\Requests\Admin\Role\CreateRequest;
	use App\Http\Requests\Admin\Role\UpdateRequest;
	use App\Model\Module;
	use Caffeinated\Shinobi\Models\Permission;
	use Caffeinated\Shinobi\Models\Role;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Str;
	use Yajra\DataTables\Facades\DataTables;

	class RoleController extends Controller {

		public function index() {
			$this->autorizate('index');
			$roles = Role::withTrashed()->get();
			return view('admin.role._index')->with(['title' => 'Panel de roles', 'class_header' => 'page-container-bg-solid', 'roles' => $roles]);
		}

		public function create(Request $request) {
			$this->autorizate('create');
			if ($request->ajax()):
				$data = view('admin.role._Create');
			else:
				$data = view('admin.role.Create')->with(['title' => 'Registro de rol']);
			endif;
			return $data;
		}

		public function store(CreateRequest $request) {
			$this->autorizate('create');
			try {
				DB::beginTransaction();
				$role = new Role($request->all());
				$role->slug = Str::slug($role->name);
				$role->save();
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 'status' => 200, 'route' => route('admin.role.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.role.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function store_permission(Request $request, $slug) {
			$this->autorizate('create_permission');
			$role = Role::where(['slug' => $slug])->first();
			if ($role):
				try {
					DB::beginTransaction();
					$role->syncPermissions(count($request->permission) > 0 ? $request->permission : []);
					$role->save();
					DB::commit();
					$message = "Registro exitoso";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 'status' => 200, 'route' => route('admin.role.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.role.permission', $role->slug), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el rol no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.role.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $slug) {
			$this->autorizate('update');
			$role = Role::where(['slug' => $slug])->first();
			if ($role):
				if ($request->ajax()):
					$data = view('admin.role._Update');
				else:
					$data = view('admin.role.Update')->with(['title' => "Actualización del rol: $role->name"]);
				endif;
				$data->with(['role' => $role]);
			else:
				if ($request->ajax()):
					$data = "Rol no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el rol no existe");
					$data = redirect(route('admin.rol.index'));
				endif;
			endif;
			return $data;
		}

		public function show_permission(Request $request, $slug) {
			$this->autorizate('create_permission');
			$role = Role::where(['slug' => $slug])->first();
			if ($role):
				$modules = Module::with('permissions')->get();
				if ($request->ajax()):
					$data = view('admin.role._Permission');
				else:
					$data = view('admin.role.Permission')->with(['title' => "Permisos para el rol: $role->name"]);
				endif;
				$role->getPermissions();
				$data->with(['modules' => $modules, 'role' => $role]);
			else:
				if ($request->ajax()):
					$data = "Rol no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el rol no existe");
					$data = redirect(route('admin.rol.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $slug) {
			$this->autorizate('update');
			$role = Role::where(['slug' => $slug])->first();
			if ($role):
				try {
					DB::beginTransaction();
					$role->fill($request->all())->save();
					DB::commit();
					$message = "Actualización exitosa";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 'status' => 200, 'route' => route('admin.role.index'), 'message' => $message, 'type' => 'success'];
					return $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.role.update', $role->name), 'message' => $message, 'type' => 'error'];
					return $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el rol no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.role.index'), 'message' => $message, 'type' => 'improper'];
				return$this->optimize($array);
			endif;
		}

		public function delete(Request $request, $slug) {
			$this->autorizate('delete');
			if ($request->ajax()):
				$role = Role::withTrashed()->where(['slug' => $slug])->first();
				if ($role):
					try {
						DB::beginTransaction();
						if ($role->deleted_at):
							$role->restore();
							$message = "Restauración exitosa";
						else:
							$role->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el rol no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.role.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $slug) {
			$this->autorizate('destroy');
			if ($request->ajax()):
				$role = Role::withTrashed()->where(['slug' => $slug])->first();
				if ($role):
					try {
						DB::beginTransaction();
						$role->forceDelete();
						DB::commit();
						$message = "Eliminación exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el rol no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.role.index'));
			endif;
			return $data;
		}

		public function table(Request $request) {
			if ($request->ajax()):
				$auth = Auth::guard('admin')->user();
				try {
					return DataTables::of(Role::withTrashed())->addColumn('link', function ($row) use ($auth) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= ($auth->can('role-permission')) ? "<li><a data-url='" . route('admin.role.permission', $row->slug) . "' class='btn-ajax'><i class='fa fa-list-alt'></i> Permisos</a></li>" : "";
							$buttons .= ($auth->can('role-update')) ? "<li><a data-url='" . route('admin.role.update', $row->slug) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
							$buttons .= ($auth->can('role-delete')) ? "<li><a data-url='" . route('admin.role.delete', $row->slug) . "' data-message='Desea deshabilitar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>" : "";
							$buttons .= ($auth->can('role-destroy')) ? "<li><a data-url='" . route('admin.role.destroy', $row->slug) . "' data-message='Desea eliminar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>" : "";
						else:
							$buttons .= ($auth->can('role-delete')) ? "<li><a data-url='" . route('admin.role.delete', $row->slug) . "' data-message='Desea restaurar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Deshabilitar</a></li>" : "";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('admin.login');
			endif;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

		private function autorizate($function) {
			//$this->authorize($function, Role::class);
		}

	}
