<?php

	namespace App\Http\Controllers\Admin\Comment;

	use App\Http\Controllers\Controller;
	use App\Http\Requests\Admin\Comment\CreateRequest;
	use App\Http\Requests\Admin\Comment\UpdateRequest;
	use App\Model\Archive;
	use App\Model\Comment;
	use App\Model\Issue;
	use Exception;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\File;
	use Illuminate\Support\Facades\Storage;

	class CommentController extends Controller {

		public function index($id) {
			$issue = Issue::with(['project.company', 'comments' => function ($query) {
				$query->withTrashed();
			}, 'comments.user'])->find($id);
			return view('admin.comment._index')->with(['title' => "Comentarios del issue: $issue->name", 'class_header' => 'page-container-bg-solid', 'issue' => $issue]);
		}

		public function create(Request $request, $id) {
			$issue = Issue::with(['project.company'])->find($id);
			if ($issue):
				if ($request->ajax()):
					$data = view('admin.comment._Create');
				else:
					$data = view('admin.comment.Create')->with(['title' => 'Registro de mensajes']);
				endif;
				$data->with(['issue' => $issue]);
			else:
				if ($request->ajax()):
					$data = "Issue no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el issue no existe");
					$data = redirect(route('admin.company.index'));
				endif;
			endif;
			return $data;
		}

		public function store(CreateRequest $request, $id) {
			$issue = Issue::find($id);
			if ($issue):
				$user = Auth::guard('admin')->user();
				try {
					DB::beginTransaction();
					$comment = new Comment($request->all());
					$comment->user_id = $user->id;
					$file = $request->file('file');
					if ($file):
						$archive = new Archive();
						$archive->name = time() . $file->getClientOriginalName();
						$archive->url = "archive/" . $archive->name;
						Storage::disk('video_archive')->put($archive->name, File::get($file));
						$archive->save();
					endif;
					$issue->comments()->save($comment);
					$message = "Registro exitoso";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.comment.index', $issue->id)], 'status' => 200, 'route' => route('admin.comment.index', $issue->id), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.comment.create', $issue->id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "El issue no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.company.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $issue_id, $comment_id) {
			$comment = Comment::where(['issue_id' => $issue_id, 'id' => $comment_id])->with('issue.project.company')->first();
			if ($comment):
				if ($request->ajax()):
					$data = view('admin.comment._Update');
				else:
					$data = view('admin.comment.Update')->with(['title' => "Actualización del mensaje del issue: " . $comment->issue->name]);
				endif;
				$data->with(['comment' => $comment]);
			else:
				if ($request->ajax()):
					$data = "Comentario no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el comentario no existe");
					$data = redirect(route('admin.company.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $issue_id, $comment_id) {
			$comment = Comment::where(['issue_id' => $issue_id, 'id' => $comment_id])->with('issue.project.company')->first();
			if ($comment):
				try {
					DB::beginTransaction();
					$comment->fill($request->all())->save();
					$message = "Actualizacion exitosa";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.comment.index', $issue_id)], 'status' => 200, 'route' => route('admin.comment.index', $issue_id), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.comment.update', $issue_id, $comment_id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el issue no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.company.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $issue_id, $comment_id) {
			if ($request->ajax()):
				$comment = Comment::withTrashed()->where(['issue_id' => $issue_id, 'id' => $comment_id])->first();
				if ($comment):
					try {
						DB::beginTransaction();
						if ($comment->deleted_at):
							$comment->restore();
							$message = "Restauracion exitosa";
						else:
							$comment->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.comment.index', $issue_id)], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el comentario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.company.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $issue_id, $comment_id) {
			if ($request->ajax()):
				$comment = Comment::withTrashed()->where(['issue_id' => $issue_id, 'id' => $comment_id])->first();
				if ($comment):
					try {
						DB::beginTransaction();
						$comment->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.comment.index', $issue_id)], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el comentario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.company.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
