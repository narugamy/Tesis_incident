<?php

	namespace App\Http\Controllers\Admin\Project;

	use App\Http\Controllers\Controller;
	use App\Http\Requests\Admin\Project\CreateRequest;
	use App\Http\Requests\Admin\Project\UpdateRequest;
	use App\Model\Company;
	use App\Model\Project;
	use Exception;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Str;
	use Yajra\DataTables\Facades\DataTables;

	class ProjectController extends Controller {

		public function index($company_slug) {
			$this->autorizate('index');
			$company = Company::where(['slug' => $company_slug])->with(['projects' => function ($query) {
				return $query->withTrashed();
			}])->first();
			if ($company):
				return view('admin.project._index')->with(['title' => "Panel de proyectos de la compañia: $company->name", 'class_header' => 'page-container-bg-solid', 'company' => $company]);
			else:
				return redirect()->route('admin.company.index');
			endif;
		}

		public function table(Request $request, $slug) {
			if ($request->ajax()):
				$company = Company::where(['slug' => $slug])->with('projects')->first();
				if ($company):
					$auth = Auth::guard('admin')->user();
					try {
						return DataTables::of(Project::where(['company_id' => $company->id])->withTrashed())->addColumn('link', function ($row) use ($auth, $company) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
								$buttons .= ($auth->can('issue-index')) ? "<li><a href='" . route('admin.issue.index', $row->slug) . "'><i class='fa fa-list-alt'></i> Issues</a></li>" : "";
								$buttons .= ($auth->can('project-update')) ? "<li><a data-url='" . route('admin.project.update', [$company->slug, $row->slug]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
								$buttons .= ($auth->can('project-delete')) ? "<li><a data-url='" . route('admin.project.delete', [$company->slug, $row->slug]) . "' data-message='Desea deshabilitar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>" : "";
								$buttons .= ($auth->can('project-destroy')) ? "<li><a data-url='" . route('admin.project.destroy', [$company->slug, $row->slug]) . "' data-message='Desea eliminar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>" : "";
							else:
								$buttons .= ($auth->can('project-delete')) ? "<li><a data-url='" . route('admin.project.delete', [$company->slug, $row->slug]) . "' data-message='Desea restaurar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>" : "";
							endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->rawColumns(['link'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				return redirect()->route('admin.login');
			endif;
		}

		public function create(Request $request, $company_slug) {
			$this->autorizate('create');
			$company = Company::where(['slug' => $company_slug])->first();
			if ($company):
				if ($request->ajax()):
					$data = view('admin.project._Create');
				else:
					$data = view('admin.project.Create')->with(['title' => 'Registro de compañia']);
				endif;
				$data->with(['company' => $company]);
			else:
				if ($request->ajax()):
					$data = "la empresa no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la empresa no existe");
					$data = redirect(route('admin.company.index'));
				endif;
			endif;
			return $data;
		}

		public function store(CreateRequest $request, $company_slug) {
			$this->autorizate('create');
			$company = Company::where(['slug' => $company_slug])->first();
			if ($company):
				try {
					DB::beginTransaction();
					$project = new Project($request->all());
					$project->slug = Str::slug($project->slug . "-" . $company->name);
					$company->projects()->save($project);
					$message = "Registro exitoso";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.project.index', $company->slug)], 'status' => 200, 'route' => route('admin.project.index', $company->slug), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.project.create', $company->slug), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "La empresa no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.company.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $company_slug, $project_slug) {
			$this->autorizate('update');
			$company = Company::where(['slug' => $company_slug])->with(['projects' => function ($query) use ($project_slug) {
				return $query->where(['slug' => $project_slug]);
			}])->first();
			if ($company):
				if (count($company->projects) > 0):
					$company->project = $company->projects[0];
					unset($company->projects);
					if ($request->ajax()):
						$data = view('admin.project._Update');
					else:
						$data = view('admin.project.Update')->with(['title' => "Actualización del proyecto: " . $company->project->name]);
					endif;
					$data->with(['company' => $company]);
				else:
					if ($request->ajax()):
						$data = "proyecto no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, el proyecto no existe");
						$data = redirect(route('admin.project.index', $company->slug));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Curso no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el curso no existe");
					$data = redirect(route('admin.course.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $company_slug, $project_slug) {
			$this->autorizate('update');
			$company = Company::where(['slug' => $company_slug])->with(['projects' => function ($query) use ($project_slug) {
				return $query->where(['slug' => $project_slug]);
			}])->first();
			if ($company):
				if (count($company->projects) > 0):
					$company->project = $company->projects[0];
					unset($company->projects);
					try {
						DB::beginTransaction();
						$company->project->fill($request->all());
						$company->project->save();
						$message = "Actualizacion exitosa";
						DB::commit();
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.project.index', $company->slug)], 'status' => 200, 'route' => route('admin.project.index', $company->slug), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.project.update', $company->slug, $company->project->slug), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					}
				else:
					$message = "No intentes algo indebido, el proyecto no existe";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.project.index', $company->slug), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = "No intentes algo indebido, la compañia no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.project.index', $company->slug), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $company_slug, $project_slug) {
			$this->autorizate('delete');
			if ($request->ajax()):
				$company = Company::where(['slug' => $company_slug])->with(['projects' => function ($query) use ($project_slug) {
					return $query->where(['slug' => $project_slug])->withTrashed();
				}])->first();
				if ($company):
					if (count($company->projects) > 0):
						$company->project = $company->projects[0];
						try {
							DB::beginTransaction();
							if ($company->project->deleted_at):
								$company->project->restore();
								$message = "Restauracion exitosa";
							else:
								$company->project->delete();
								$message = "Deshabilitación exitosa";
							endif;
							DB::commit();
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.project.index', $company->slug)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la compañia no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.company.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $company_slug, $project_slug) {
			$this->autorizate('destroy');
			if ($request->ajax()):
				$company = Company::where(['slug' => $company_slug])->with(['projects' => function ($query) use ($project_slug) {
					return $query->where(['slug' => $project_slug]);
				}])->first();
				if ($company):
					if (count($company->projects) > 0):
						$company->project = $company->projects[0];
						try {
							DB::beginTransaction();
							$company->project->forceDelete();
							DB::commit();
							$message = "Eliminacion exitosa";
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.project.index', $company_slug)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la compañia no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.company.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

		private function autorizate($function) {
			//$this->authorize($function, Company::class);
		}

	}
