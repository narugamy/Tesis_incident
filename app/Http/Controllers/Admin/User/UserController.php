<?php

	namespace App\Http\Controllers\Admin\User;

	use App\Http\Requests\Admin\User\CreateRequest;
	use App\Http\Requests\Admin\User\UpdateRequest;
	use App\Model\Company;
	use App\Model\Module;
	use App\Model\User;
	use Caffeinated\Shinobi\Models\Role;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class UserController extends Controller {

		public function index() {
			$this->autorizate('index');
			$users = User::withTrashed()->get();
			return view('admin.user._index')->with(['title' => 'Panel de usuarios', 'class_header' => 'page-container-bg-solid', 'users' => $users]);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				$auth = Auth::guard('admin')->user();
				try {
					return DataTables::of(User::withTrashed())->addColumn('link', function ($row) use ($auth) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= ($auth->can('user-permission')) ? "<li><a data-url='" . route('admin.user.permission', $row->username) . "' class='btn-ajax'><i class='fa fa-list-alt'></i> Permisos</a></li>" : "";
							$buttons .= ($auth->can('user-update')) ? "<li><a data-url='" . route('admin.user.update', $row->username) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
							$buttons .= ($auth->can('user-delete')) ? "<li><a data-url='" . route('admin.user.delete', $row->username) . "' data-message='Desea deshabilitar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>" : "";
							$buttons .= ($auth->can('user-destroy')) ? "<li><a data-url='" . route('admin.user.destroy', $row->username) . "' data-message='Desea eliminar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>" : "";
						else:
							$buttons .= ($auth->can('user-delete')) ? "<li><a data-url='" . route('admin.user.delete', $row->username) . "' data-message='Desea restaurar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Deshabilitar</a></li>" : "";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addColumn('full_name', function ($row) {
						return $row->full_name();
					})->addColumn('role', function ($row) {
						return $row->getRoles()[0];
					})->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('admin.login');
			endif;
		}

		public function create(Request $request) {
			$this->autorizate('create');
			$roles = Role::orderBy('name')->pluck('name', 'id');
			$companies = Company::orderBy('name')->pluck('name', 'id');
			if ($request->ajax()):
				$data = view('admin.user._Create');
			else:
				$data = view('admin.user.Create')->with(['title' => 'Registro de usuario']);
			endif;
			return $data->with(['roles' => $roles, 'companies' => $companies]);
		}

		public function store(CreateRequest $request) {
			$this->autorizate('create');
			try {
				DB::beginTransaction();
				$user = new User($request->all());
				$user->password = bcrypt($user->password);
				$user->save();
				$user->assignRole($request->role_id);
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function store_permission(Request $request, $username) {
			$this->autorizate('create_permission');
			$user = User::where(['username' => $username])->first();
			if ($user):
				try {
					DB::beginTransaction();
					$user->syncPermissions(count($request->permission) > 0 ? $request->permission : []);
					$user->save();
					DB::commit();
					$message = "Registro exitoso";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.permission', $user->username), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el usuario no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $username) {
			$this->autorizate('update');
			$user = User::where(['username' => $username])->first();
			if ($user):
				$role = Role::where(['name' => $user->getRoles()[0]])->first();
				$roles = Role::orderBy('name')->pluck('name', 'id');
				$companies = Company::orderBy('name')->pluck('name', 'id');
				if ($request->ajax()):
					$data = view('admin.user._Update');
				else:
					$data = view('admin.user.Update')->with(['title' => "Actualización del usuario: ".$user->full_name()]);
				endif;
				$data->with(['user' => $user, 'role' => $role, 'roles' => $roles, 'companies' => $companies]);
			else:
				if ($request->ajax()):
					$data = "Usuario no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el usuario no existe");
					$data = redirect(route('admin.user.index'));
				endif;
			endif;
			return $data;
		}

		public function show_permission(Request $request, $username) {
			$this->autorizate('create_permission');
			$user = User::where(['username' => $username])->first();
			if ($user):
				$modules = Module::with('permissions')->get();
				if ($request->ajax()):
					$data = view('admin.user._Permission');
				else:
					$data = view('admin.user.Permission')->with(['title' => "Permisos para el usuario: $user->full_name()"]);
				endif;
				$data->with(['modules' => $modules, 'user' => $user]);
			else:
				if ($request->ajax()):
					$data = "Usuario no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el usuario no existe");
					$data = redirect(route('admin.user.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $username) {
			$this->autorizate('update');
			$user = User::where(['username' => $username])->first();
			if ($user):
				try {
					DB::beginTransaction();
					$password = $user->password;
					$user->fill($request->all());
					$user->password = (!empty($user->password)) ? bcrypt($request->password) : $password;
					$user->syncRoles([$request->role_id]);
					$user->save();
					DB::commit();
					$message = "Actualización exitosa";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.update', $user->username), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el usuario no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $username) {
			$this->autorizate('delete');
			if ($request->ajax()):
				$user = User::withTrashed()->where(['username' => $username])->first();
				if ($user):
					try {
						DB::beginTransaction();
						if ($user->deleted_at):
							$user->restore();
							$message = "Restauración exitosa";
						else:
							$user->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el usuario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.user.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $username) {
			$this->autorizate('destroy');
			if ($request->ajax()):
				$user = User::withTrashed()->where(['username' => $username])->first();
				if ($user):
					try {
						DB::beginTransaction();
						$user->forceDelete();
						DB::commit();
						$message = "Eliminación exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el usuario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.user.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

		private function autorizate($function) {
			//$this->authorize($function, User::class);
		}

	}
