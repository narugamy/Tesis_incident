<?php

	namespace App\Http\Controllers\Developer\Company;

	use App\Http\Requests\Admin\Company\CreateRequest;
	use App\Http\Requests\Admin\Company\UpdateRequest;
	use App\Model\Company;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Str;
	use Yajra\DataTables\Facades\DataTables;

	class CompanyController extends Controller {

		public function index() {
			$companies = Company::all();
			return view('developer.company._index')->with(['title' => 'Panel de compañias', 'class_header' => 'page-container-bg-solid', 'companies' => $companies]);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				try {
					return DataTables::of(Company::withTrashed())->addColumn('link', function ($row) use ($auth) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= ($auth->can('project-index')) ? "<li><a href='" . route('developer.project.index', $row->slug) . "'><i class='fa fa-list-alt'></i> Proyectos</a></li>" : "";
							$buttons .= ($auth->can('company-update')) ? "<li><a data-url='" . route('developer.company.update', $row->slug) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
							$buttons .= ($auth->can('company-delete')) ? "<li><a data-url='" . route('developer.company.delete', $row->slug) . "' data-message='Desea deshabilitar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>" : "";
							$buttons .= ($auth->can('company-destroy')) ? "<li><a data-url='" . route('developer.company.destroy', $row->slug) . "' data-message='Desea eliminar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>" : "";
						else:
							$buttons .= ($auth->can('company-delete')) ? "<li><a data-url='" . route('developer.company.delete', $row->slug) . "' data-message='Desea restaurar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Deshabilitar</a></li>" : "";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('developer.login');
			endif;
		}

		public function create(Request $request) {
			if ($request->ajax()):
				$data = view('developer.company._Create');
			else:
				$data = view('developer.company.Create')->with(['title' => 'Registro de compañia']);
			endif;
			return $data;
		}

		public function store(CreateRequest $request) {
			try {
				DB::beginTransaction();
				$company = new Company($request->all());
				$company->slug = Str::slug($company->name);
				$company->save();
				$message = "Registro exitoso";
				DB::commit();
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.company.index')], 'status' => 200, 'route' => route('developer.company.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.company.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $slug) {
			$company = Company::where(['slug' => $slug])->first();
			if ($company):
				if ($request->ajax()):
					$data = view('developer.company._Update');
				else:
					$data = view('developer.company.Update')->with(['title' => "Actualización de la compañia: $company->name"]);
				endif;
				$data->with(['company' => $company]);
			else:
				if ($request->ajax()):
					$data = "Compañia no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la compañia no existe");
					$data = redirect(route('developer.company.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $slug) {
			$company = Company::withTrashed()->where(['slug' => $slug])->first();
			if ($company):
				try {
					DB::beginTransaction();
					$company->fill($request->all());
					$company->save();
					$message = "Actualizacion exitosa";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.company.index')], 'status' => 200, 'route' => route('developer.company.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.company.update', $company->id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, la compañia no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.company.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $slug) {
			if ($request->ajax()):
				$company = Company::withTrashed()->where(['slug' => $slug])->first();
				if ($company):
					try {
						DB::beginTransaction();
						if ($company->deleted_at):
							$company->restore();
							$message = "Restauracion exitosa";
						else:
							$company->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.company.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la compañia no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.company.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $slug) {
			if ($request->ajax()):
				$company = Company::where(['slug' => $slug])->first();
				if ($company):
					try {
						DB::beginTransaction();
						$company->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.company.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la compañia no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.company.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
