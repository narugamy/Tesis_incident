<?php

	namespace App\Http\Controllers\Developer\Assign;

	use App\Model\Archive;
	use App\Model\Comment;
	use App\Model\Issue;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\File;
	use Illuminate\Support\Facades\Storage;

	class CommentController extends Controller {

		public function index($id) {
			$auth = Auth::guard('developer')->user();
			$issue = Issue::where(['user_id' => $auth->id])->with(['project.company', 'comments' => function ($query) {
				$query->withTrashed();
			}, 'comments.user','comments.archive'])->find($id);
			if ($issue):
				return view('developer.assign.comment._index')->with(['title' => "Comentarios del issue: $issue->name", 'class_header' => 'page-container-bg-solid', 'issue' => $issue]);
			else:
				return redirect()->route('developer.assign.issue.index');
			endif;
		}

		public function create(Request $request, $id) {
			$auth = Auth::guard('developer')->user();
			$issue = Issue::where(['user_id' => $auth->id])->with(['project.company'])->find($id);
			if ($issue):
				if ($request->ajax()):
					$data = view('developer.assign.issue._Create');
				else:
					$data = view('developer.assign.issue.Create')->with(['title' => "Registro de mensajes del Issue: $issue->name del proyecto: " . $issue->project->name . " de la empresa: " . $issue->project->company->name]);
				endif;
				$data->with(['issue' => $issue]);
			else:
				if ($request->ajax()):
					$data = "Issue no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el issue no existe");
					$data = redirect(route('developer.assign.issue.index'));
				endif;
			endif;
			return $data;
		}

		public function store(Request $request, $id) {
			$auth = Auth::guard('developer')->user();
			$issue = Issue::where(['user_id' => $auth->id])->with(['project.company'])->find($id);
			if ($issue):
				try {
					DB::beginTransaction();
					$comment = new Comment($request->all());
					$comment->user_id = $auth->id;
					$file = $request->file('file');
					if ($file):
						$archive = new Archive();
						$archive->name = time() . $file->getClientOriginalName();
						$archive->url = "archive/" . $archive->name;
						Storage::disk('video_archive')->put($archive->name, File::get($file));
						$archive->save();
					endif;
					$issue->comments()->save($comment);
					$message = "Registro exitoso";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.assign.comment.index', $issue->id)], 'status' => 200, 'route' => route('developer.assign.comment.index', $issue->id), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.assign.comment.create', $issue->id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "El issue no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.assign.issue.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $issue_id, $comment_id) {
			$auth = Auth::guard('developer')->user();
			$issue = Issue::where(['user_id' => $auth->id])->with(['project.company'])->find($issue_id);
			if ($issue):
				$comment = Comment::withTrashed()->where(['issue_id' => $issue->id, 'id' => $comment_id])->first();
				if ($comment):
					if ($request->ajax()):
						$data = view('developer.assign.comment._Update');
					else:
						$data = view('developer.assign.comment.Update')->with(['title' => "Actualización del mensaje del issue: " . $comment->issue->name]);
					endif;
					$data->with(['comment' => $comment, 'issue' => $issue]);
				else:
					if ($request->ajax()):
						$data = "Comentario no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, el comentario no existe");
						$data = redirect(route('developer.assign.comment.index', $issue->id));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Comentario no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el issue no existe");
					$data = redirect(route('developer. assign.issue.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $issue_id, $comment_id) {
			$auth = Auth::guard('developer')->user();
			$issue = Issue::where(['user_id' => $auth->id])->with(['project.company'])->find($issue_id);
			if ($issue):
				$comment = Comment::where(['issue_id' => $issue->id, 'id' => $comment_id])->first();
				if ($comment):
					try {
						DB::beginTransaction();
						$comment->fill($request->all())->save();
						$message = "Actualizacion exitosa";
						DB::commit();
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.assign.comment.index', $issue->id)], 'status' => 200, 'route' => route('developer.assign.comment.index', $issue->id), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.assign.comment.update', [$issue_id, $comment_id]), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					}
				else:
					$message = "No intentes algo indebido, el comentario no existe";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.assign.comment.index', $issue->id), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = "No intentes algo indebido, el issue no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.assign.issue.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $issue_id, $comment_id) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				$issue = Issue::where(['user_id' => $auth->id])->with(['project.company'])->find($issue_id);
				if ($issue):
					$comment = Comment::withTrashed()->where(['issue_id' => $issue->id, 'id' => $comment_id])->first();
					if ($comment):
						try {
							DB::beginTransaction();
							if ($comment->deleted_at):
								$comment->restore();
								$message = "Restauracion exitosa";
							else:
								$comment->delete();
								$message = "Deshabilitación exitosa";
							endif;
							DB::commit();
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.assign.comment.index', $issue_id)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el comentario no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el issue no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.assign.issue.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $issue_id, $comment_id) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				$issue = Issue::where(['user_id' => $auth->id])->with(['project.company'])->find($issue_id);
				if ($issue):
					$comment = Comment::withTrashed()->where(['issue_id' => $issue->id, 'id' => $comment_id])->first();
					if ($comment):
						try {
							DB::beginTransaction();
							$comment->forceDelete();
							DB::commit();
							$message = "Eliminacion exitosa";
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.assign.comment.index', $issue_id)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el comentario no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el issue no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.assign.issue.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
