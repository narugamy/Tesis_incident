<?php

	namespace App\Http\Controllers\Developer\Assign;

	use App\Model\Issue;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class IssueController extends Controller {

		public function index() {
			$auth = Auth::guard('developer')->user();
			$issues = Issue::where(['user_id' => $auth->id])->with('project.company')->get();
			if ($issues):
				return view('developer.assign.issue._index')->with(['title' => "Panel de issues asignados", 'issues' => $issues]);
			else:
				return redirect()->route('developer.assign.issues.index');
			endif;
		}

		public function table(Request $request) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				try {
					return DataTables::of(Issue::where(['user_id' => $auth->id])->orderBy('project_id')->with('project.company'))->addColumn('link', function ($row) use ($auth) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if ((int)$row->status === 0):
							$buttons .= "<li><a data-url='" . route('developer.assign.issue.close', [$row->id]) . "' data-message='Desea cerrar el issue: " . $row->name . " por estar solucionado' class='btn-destroy'><i class='fa fa-remove'></i> Cerrar</a></li>";
							$buttons .= "<li><a href='" . route('developer.assign.comment.index', $row->id) . "'><i class='fa fa-list-alt'></i> Comentarios</a></li>";
							$buttons .= ($auth->can('issue-update')) ? "<li><a data-url='" . route('developer.assign.issue.update', $row->id) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
							$buttons .= "</ul></div>";
						else:
							$buttons .= "<li><a href='" . route('developer.assign.comment.index', $row->id) . "'><i class='fa fa-list-alt'></i> Comentarios</a></li>";
						endif;
						return $buttons;
					})->addIndexColumn()->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('developer.login');
			endif;
		}

		public function close(Request $request, $issue_id) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				$issue = Issue::where(['user_id' => $auth->id])->with(['project.company'])->find($issue_id);
				if ($issue):
					try {
						DB::beginTransaction();
						if ((int)$issue->status === 0):
							$issue->fill(['status' => 1])->save();
							$message = "Cerrado exitosa";
						else:
							$issue->fill(['status' => 0])->save();
							$message = "Abierto exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.assign.issue.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el issue no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.assign.issue.index'));
			endif;
			return $data;
		}

	}
