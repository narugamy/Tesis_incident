<?php

	namespace App\Http\Controllers\Developer\My;

	use App\Model\Issue;
	use App\Model\Project;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class IssueController extends Controller {

		public function index() {
			return view('developer.my.issue._index')->with(['title' => "Panel de mis issues"]);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				try {
					return DataTables::of(Issue::where(['client_id' => $auth->id])->withTrashed()->with('project.company'))->addColumn('link', function ($row) use ($auth) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a href='" . route('developer.my.comment.index', $row->id) . "'><i class='fa fa-list-alt'></i> Comentarios</a></li>";
							$buttons .= "<li><a data-url='" . route('developer.my.issue.update', [$row->id]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							$buttons .= "<li><a data-url='" . route('developer.my.issue.delete', [$row->id]) . "' data-message='Desea deshabilitar el issue: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
							$buttons .= "<li><a data-url='" . route('developer.my.issue.destroy', [$row->id]) . "' data-message='Desea eliminar el issue: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
						else:
							$buttons .= "<li><a data-url='" . route('developer.my.issue.delete', [$row->id]) . "' data-message='Desea restaurar el issue: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addIndexColumn()->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('developer.login');
			endif;
		}

		public function create(Request $request) {
			$projects = Project::orderBy('name')->pluck('name', 'id');
			if ($request->ajax()):
				$data = view('developer.my.issue._Create');
			else:
				$data = view('developer.my.issue.Create')->with(['title' => 'Registro de Issue']);
			endif;
			return $data->with(['projects' => $projects]);
		}

		public function store(Request $request) {
			$user = Auth::guard('developer')->user();
			try {
				DB::beginTransaction();
				$issue = new Issue($request->all());
				$issue->client_id = $user->id;
				$issue->code = $this->code($user);
				$issue->save();
				$message = "Registro exitoso";
				DB::commit();
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.my.issue.index')], 'status' => 200, 'route' => route('developer.my.issue.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my.issue.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $issue_id) {
			$auth = Auth::guard('developer')->user();
			$issue = Issue::where(['client_id' => $auth->id])->withTrashed()->with('project.company')->find($issue_id);
			if ($issue):
				$projects = Project::orderBy('name')->pluck('name', 'id');
				if ($request->ajax()):
					$data = view('developer.my.issue._Update');
				else:
					$data = view('developer.my.issue.Update')->with(['title' => "Actualización del issue: " . $issue->name]);
				endif;
				$data->with(['issue' => $issue, 'projects' => $projects]);
			else:
				if ($request->ajax()):
					$data = "issue no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el issue no existe");
					$data = redirect(route('developer.my.issue.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $issue_id) {
			$auth = Auth::guard('developer')->user();
			$issue = Issue::where(['client_id' => $auth->id])->withTrashed()->with('project.company')->find($issue_id);
			if ($issue):
				try {
					DB::beginTransaction();
					$issue->fill($request->all());
					$issue->save();
					$message = "Actualizacion exitosa";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.my.issue.index')], 'status' => 200, 'route' => route('developer.my.issue.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my.issue.update', $issue->id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el issue no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my.issue.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $issue_id) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				$issue = Issue::where(['client_id' => $auth->id])->withTrashed()->with('project.company')->find($issue_id);
				if ($issue):
					try {
						DB::beginTransaction();
						if ($issue->deleted_at):
							$issue->restore();
							$message = "Restauracion exitosa";
						else:
							$issue->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.my.issue.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el issue no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.my.issue.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $issue_id) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				$issue = Issue::where(['client_id' => $auth->id])->withTrashed()->with('project.company')->find($issue_id);
				if ($issue):
					try {
						DB::beginTransaction();
						$issue->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.my.issue.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el issue no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.my.issue.index'));
			endif;
			return $data;
		}

		private function code($user) {
			$code = Issue::whereClient_id($user->id)->max('code');
			return empty($code) ? '000000000001' : str_pad((int)$code + 1, 12, "0", STR_PAD_LEFT);
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
