<?php

	namespace App\Http\Controllers\Developer\Project;

	use App\Http\Requests\Developer\Project\CreateRequest;
	use App\Http\Requests\Developer\Project\UpdateRequest;
	use App\Model\Company;
	use App\Model\Project;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Str;
	use Yajra\DataTables\Facades\DataTables;

	class ProjectController extends Controller {

		public function index() {
			$project = Project::with('company')->get();
			if ($project):
				return view('developer.project._index')->with(['title' => "Panel de proyectos", 'class_header' => 'page-container-bg-solid', 'project' => $project]);
			else:
				return redirect()->route('developer.project.index');
			endif;
		}

		public function table(Request $request) {
			if ($request->ajax()):
				$auth = Auth::guard('developer')->user();
				try {
					return DataTables::of(Project::with(['company', 'issues' => function ($query) use ($auth) {
						return $query->where('client_id', '!=', $auth->id)->whereNull('user_id');
					}]))->addColumn('link', function ($row) use ($auth) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							if (count($row->issues) > 0):
								$buttons .= ($auth->can('issue-index')) ? "<li><a href='" . route('developer.issue.index', $row->slug) . "'><i class='fa fa-list-alt'></i> Issues</a></li>" : "";
							endif;
							$buttons .= ($auth->can('project-update')) ? "<li><a data-url='" . route('developer.project.update', [$row->slug]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
							$buttons .= ($auth->can('project-delete')) ? "<li><a data-url='" . route('developer.project.delete', [$row->slug]) . "' data-message='Desea deshabilitar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>" : "";
							$buttons .= ($auth->can('project-destroy')) ? "<li><a data-url='" . route('developer.project.destroy', [$row->slug]) . "' data-message='Desea eliminar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>" : "";
						else:
							$buttons .= ($auth->can('project-delete')) ? "<li><a data-url='" . route('developer.project.delete', [$row->slug]) . "' data-message='Desea restaurar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>" : "";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addColumn('news', function ($row) {
						return count($row->issues);
					})->addIndexColumn()->rawColumns(['link', 'news'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('developer.login');
			endif;
		}

		public function create(Request $request) {
			$companies = Company::orderBy('name')->pluck('name', 'id');
			if ($request->ajax()):
				$data = view('developer.project._Create');
			else:
				$data = view('developer.project.Create')->with(['title' => 'Registro de proyecto']);
			endif;
			return $data->with(['companies' => $companies]);
		}

		public function store(CreateRequest $request) {
			try {
				DB::beginTransaction();
				$company = Company::find($request->company_id);
				$project = new Project($request->all());
				$project->slug = Str::slug($project->name . "-" . $company->name);
				$project->save();
				$message = "Registro exitoso";
				DB::commit();
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.project.index')], 'status' => 200, 'route' => route('developer.project.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.project.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $project_slug) {
			$project = Project::where(['slug' => $project_slug])->first();
			if ($project):
				$companies = Company::orderBy('name')->pluck('name', 'id');
				if ($request->ajax()):
					$data = view('developer.project._Update');
				else:
					$data = view('developer.project.Update')->with(['title' => "Actualización del proyecto: " . $project->name]);
				endif;
				$data->with(['project' => $project, 'companies' => $companies]);
			else:
				if ($request->ajax()):
					$data = "Curso no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el curso no existe");
					$data = redirect(route('developer.project.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $project_slug) {
			$project = Project::where(['slug' => $project_slug])->first();
			if ($project):
				try {
					DB::beginTransaction();
					$project->fill($request->all());
					$project->save();
					$message = "Actualizacion exitosa";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.project.index')], 'status' => 200, 'route' => route('developer.project.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.project.update', $project->slug), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el proyecto no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.project.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $project_slug) {
			if ($request->ajax()):
				$project = Project::where(['slug' => $project_slug])->first();
				if ($project):
					try {
						DB::beginTransaction();
						if ($project->deleted_at):
							$project->restore();
							$message = "Restauracion exitosa";
						else:
							$project->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.project.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.project.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $project_slug) {
			if ($request->ajax()):
				$project = Project::where(['slug' => $project_slug])->first();
				if ($project):
					try {
						DB::beginTransaction();
						$project->project->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.project.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.project.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
