<?php

	namespace App\Http\Controllers\Developer\Issue;

	use App\Model\Issue;
	use App\Model\Project;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class IssueController extends Controller {

		public function index($slug) {
			$project = Project::where(['slug' => $slug])->with(['company', 'issues' => function ($query) {
				return $query->whereNull('user_id');
			}])->first();
			if ($project):
				return view('developer.issue._index')->with(['title' => "Panel de issues del proyecto: $project->name", 'project' => $project]);
			else:
				return redirect()->route('developer.project.index');
			endif;
		}

		public function table(Request $request, $slug) {
			if ($request->ajax()):
				$project = Project::where(['slug' => $slug])->with('issues')->first();
				if ($project):
					$auth = Auth::guard('developer')->user();
					try {
						return DataTables::of(Issue::where(['project_id' => $project->id])->where('user_id','!=', $auth->id)->whereNull('user_id'))->addColumn('link', function ($row) use ($auth, $project) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
									$buttons .= "<li><a data-url='" . route('developer.issue.assign', [$project->slug, $row->id]) . "' class='btn-ajax'><i class='fa fa-flag'></i> Asignar</a></li>";
									$buttons .= ($auth->can('issue-update')) ? "<li><a data-url='" . route('developer.issue.update', [$project->slug, $row->id]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>" : "";
									$buttons .= ($auth->can('issue-delete')) ? "<li><a data-url='" . route('developer.issue.delete', [$project->slug, $row->id]) . "' data-message='Desea deshabilitar el issue: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>" : "";
									$buttons .= ($auth->can('issue-destroy')) ? "<li><a data-url='" . route('developer.issue.destroy', [$project->slug, $row->id]) . "' data-message='Desea eliminar el issue: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>" : "";
								else:
									$buttons .= ($auth->can('issue-delete')) ? "<li><a data-url='" . route('developer.issue.delete', [$project->slug, $row->id]) . "' data-message='Desea restaurar el issue: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>" : "";
								endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->addIndexColumn()->rawColumns(['link'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				return redirect()->route('developer.login');
			endif;
		}

		public function create(Request $request, $slug) {
			$project = Project::where(['slug' => $slug])->whereNull('user_id')->first();
			if ($project):
				if ($request->ajax()):
					$data = view('developer.issue._Create');
				else:
					$data = view('developer.issue.Create')->with(['title' => 'Registro de Issue']);
				endif;
				$data->with(['project' => $project]);
			else:
				if ($request->ajax()):
					$data = "la empresa no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el proyecto no existe");
					$data = redirect(route('developer.company.index'));
				endif;
			endif;
			return $data;
		}

		public function show_assign(Request $request, $project_slug, $issue_id) {
			if ($request->ajax()):
				$issue = Issue::with(['project' => function ($query) use ($project_slug) {
					return $query->where(['slug' => $project_slug]);
				}])->find($issue_id);
				if ($issue):
					if ($issue->project):
						$data = view('developer.issue._Show')->with(['issue' => $issue]);
					else:
						$data = "El proyecto no existe";
					endif;
				else:
					$data = "El issue no existente";
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.project.index'));
			endif;
			return $data;
		}

		public function assign(Request $request, $project_slug, $issue_id) {
			if ($request->ajax()):
				$issue = Issue::with(['project' => function ($query) use ($project_slug) {
					return $query->where(['slug' => $project_slug]);
				}])->find($issue_id);
				if ($issue):
					if ($issue->project):
						if (!$issue->user_id):
							try {
								DB::beginTransaction();
								$user = Auth::guard('developer')->user();
								$issue->fill(['user_id' => $user->id])->save();
								DB::commit();
								$message = "Asignación exitosa";
								session()->flash('success', $message);
								$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.issue.index', $project_slug)], 200);
							} catch (Exception $e) {
								DB::rollBack();
								$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
							}
						else:
							$message = "El issue ya esta asignado por otro usuario";
							session()->flash('improper', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.issue.index', $project_slug)], 200);
						endif;
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la incidencia no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.project.index'));
			endif;
			return $data;
		}

//////////////////////////////////////////////////////////////////////////////////
		/*
				public function store(CreateRequest $request, $slug) {
					$project = Project::where(['slug' => $slug])->first();
					if ($project):
						$user = Auth::guard('developer')->user();
						try {
							DB::beginTransaction();
							$issue = new Issue($request->all());
							$issue->client_id = $user->id;
							$issue->code = $this->code($user);
							$project->issues()->save($issue);
							$message = "Registro exitoso";
							DB::commit();
							$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.issue.index', $project->slug)], 'status' => 200, 'route' => route('developer.issue.index', $project->slug), 'message' => $message, 'type' => 'success'];
							$data = $this->optimize($array);
						} catch (Exception $e) {
							DB::rollBack();
							$message = "Ocurrio un error en el proceso";
							$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.issue.create', $project->slug), 'message' => $message, 'type' => 'error'];
							$data = $this->optimize($array);
						}
					else:
						$message = "La empresa no existe";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.company.index'), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					endif;
					return $data;
				}

				public function show(Request $request, $project_slug, $issue_id) {
					$project = Project::where(['slug' => $project_slug])->with(['issues' => function ($query) use ($issue_id) {
						return $query->find($issue_id);
					}])->first();
					if ($project):
						if (count($project->issues) > 0):
							$project->issue = $project->issues[0];
							unset($project->issues);
							if ($request->ajax()):
								$data = view('developer.issue._Update');
							else:
								$data = view('developer.issue.Update')->with(['title' => "Actualización del issue: " . $project->issue->name]);
							endif;
							$data->with(['project' => $project]);
						else:
							if ($request->ajax()):
								$data = "issue no existente";
							else:
								session()->flash('improper', "No intentes algo indebido, el issue no existe");
								$data = redirect(route('developer.issue.index', $project->slug));
							endif;
						endif;
					else:
						if ($request->ajax()):
							$data = "Proyecto no existente";
						else:
							session()->flash('improper', "No intentes algo indebido, el proyecto no existe");
							$data = redirect(route('developer.company.index'));
						endif;
					endif;
					return $data;
				}

				public function update(UpdateRequest $request, $project_slug, $issue_id) {
					$project = Project::where(['slug' => $project_slug])->with(['issues' => function ($query) use ($issue_id) {
						return $query->find($issue_id);
					}])->first();
					if ($project):
						if (count($project->issues) > 0):
							$project->issue = $project->issues[0];
							unset($project->projects);
							try {
								DB::beginTransaction();
								$project->issue->fill($request->all());
								$project->issue->save();
								$message = "Actualizacion exitosa";
								DB::commit();
								$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.issue.index', $project->slug)], 'status' => 200, 'route' => route('developer.issue.index', $project->slug), 'message' => $message, 'type' => 'success'];
								$data = $this->optimize($array);
							} catch (Exception $e) {
								DB::rollBack();
								$message = "Ocurrio un error en el proceso";
								$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.issue.update', $project->slug, $project->issue->id), 'message' => $message, 'type' => 'error'];
								$data = $this->optimize($array);
							}
						else:
							$message = "No intentes algo indebido, el issue no existe";
							$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.issue.index', $project->slug), 'message' => $message, 'type' => 'improper'];
							$data = $this->optimize($array);
						endif;
					else:
						$message = "No intentes algo indebido, el proyecto no existe";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.company.index'), 'message' => $message, 'type' => 'improper'];
						$data = $this->optimize($array);
					endif;
					return $data;
				}

				public function delete(Request $request, $project_slug, $issue_id) {
					if ($request->ajax()):
						$project = Project::where(['slug' => $project_slug])->with(['issues' => function ($query) use ($issue_id) {
							return $query->withTrashed()->find($issue_id);
						}])->first();
						if ($project):
							if (count($project->issues) > 0):
								$project->issue = $project->issues[0];
								try {
									DB::beginTransaction();
									if ($project->issue->deleted_at):
										$project->issue->restore();
										$message = "Restauracion exitosa";
									else:
										$project->issue->delete();
										$message = "Deshabilitación exitosa";
									endif;
									DB::commit();
									session()->flash('success', $message);
									$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.issue.index', $project->slug)], 200);
								} catch (Exception $e) {
									DB::rollBack();
									$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
								}
							else:
								$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el issue no existe"], 422);
							endif;
						else:
							$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
						endif;
					else:
						session()->flash('improper', "No intentes algo indebido");
						$data = redirect(route('developer.company.index'));
					endif;
					return $data;
				}

				public function destroy(Request $request, $project_slug, $issue_id) {
					if ($request->ajax()):
						$project = Project::where(['slug' => $project_slug])->with(['issues' => function ($query) use ($issue_id) {
							return $query->find($issue_id);
						}])->first();
						if ($project):
							if (count($project->issues) > 0):
								$project->issue = $project->issues[0];
								try {
									DB::beginTransaction();
									$project->issue->forceDelete();
									DB::commit();
									$message = "Eliminacion exitosa";
									session()->flash('success', $message);
									$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.issue.index', $project_slug)], 200);
								} catch (Exception $e) {
									DB::rollBack();
									$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
								}
							else:
								$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el issue no existe"], 422);
							endif;
						else:
							$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
						endif;
					else:
						session()->flash('improper', "No intentes algo indebido");
						$data = redirect(route('developer.company.index'));
					endif;
					return $data;
				}
		*/
		private function code($user) {
			$code = Issue::whereClient_id($user->id)->max('code');
			return empty($code) ? '000000000001' : str_pad((int)$code + 1, 12, "0", STR_PAD_LEFT);
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
