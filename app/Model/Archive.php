<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Archive extends Model {

		use SoftDeletes;

		protected $table = "archive";

		protected $fillable = ['name', 'url'];

		public function comment() {
			return $this->belongsTo(User::class);
		}

	}
