<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Issue extends Model {

		use SoftDeletes;

		protected $table = "issue";

		protected $fillable = ['project_id', 'client_id', 'user_id', 'code', 'name', 'description', 'status'];

		public function comments() {
			return $this->hasMany(Comment::class);
		}

		public function user() {
			return $this->belongsTo(User::class);
		}

		public function client() {
			return $this->belongsTo(User::class, 'client_id');
		}

		public function project() {
			return $this->belongsTo(Project::class);
		}

	}
