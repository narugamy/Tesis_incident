<?php

	namespace App\Model;

	use Caffeinated\Shinobi\Models\Permission;
	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Module extends Model {

		use SoftDeletes;

		protected $table = "module";

		protected $fillable = ['name', 'slug'];

		public function permissions() {
			return $this->hasMany(Permission::class);
		}

	}
