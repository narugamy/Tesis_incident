<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Image extends Model {

		use SoftDeletes;

		protected $table = "image";

		protected $fillable = ['name', 'url'];

		public function user() {
			return $this->belongsTo(User::class);
		}

	}
