<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Comment extends Model {

		use SoftDeletes;

		protected $table = "comment";

		protected $fillable = ['issue_id', 'user_id', 'archive_id', 'description'];

		public function user() {
			return $this->belongsTo(User::class);
		}

		public function issue() {
			return $this->belongsTo(Issue::class);
		}

		public function archive() {
			return $this->belongsTo(Archive::class);
		}

	}
