<?php

	namespace App\Model;

	use Caffeinated\Shinobi\Traits\ShinobiTrait;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Notifications\Notifiable;
	use Illuminate\Foundation\Auth\User as Authenticatable;

	class User extends Authenticatable {

		use Notifiable, SoftDeletes, ShinobiTrait;

		protected $table = "user";

		protected $fillable = ['image_id', 'company_id', 'names', 'surnames', 'address', 'email', 'username', 'password'];

		protected $hidden = ['password', 'remember_token',];

		public function full_name() {
			return "$this->names $this->surnames";
		}

		public function image() {
			return $this->hasOne(Image::class);
		}

		public function company() {
			return $this->belongsTo(Company::class);
		}

		public function issues() {
			return $this->hasMany(Issue::class);
		}

		public function comments() {
			return $this->hasMany(Comment::class);
		}

	}
