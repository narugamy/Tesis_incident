<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Project extends Model {

		use SoftDeletes;

		protected $table = "project";

		protected $fillable = ['company_id', 'name', 'description', 'slug'];

		public function issues() {
			return $this->hasMany(Issue::class);
		}

		public function company() {
			return $this->belongsTo(Company::class);
		}

	}
