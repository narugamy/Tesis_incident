<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;

	class CreatePermissionTable extends Migration {

		public function up() {
			Schema::create('permission', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('module_id')->unsigned()->index();
				$table->foreign('module_id')->references('id')->on('module')->onDelete('cascade');
				$table->string('name');
				$table->string('route')->unique();
				$table->string('slug')->unique();
				$table->text('description')->nullable();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::drop('permission');
		}

	}
