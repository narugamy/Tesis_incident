<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreatePermissionUserTable extends Migration {

		public function up() {
			Schema::create('permission_user', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('permission_id')->unsigned()->index();
				$table->foreign('permission_id')->references('id')->on('permission')->onDelete('cascade');
				$table->integer('user_id')->unsigned()->index();
				$table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
				$table->timestamps();
			});
		}

		public function down() {
			Schema::drop('permission_user');
		}

	}
