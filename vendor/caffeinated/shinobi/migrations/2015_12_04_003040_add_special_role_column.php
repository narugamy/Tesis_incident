<?php

	use Illuminate\Database\Migrations\Migration;

	class AddSpecialRoleColumn extends Migration {

		public function up() {
			Schema::table('role', function ($table) {
				$table->enum('special', ['all-access', 'no-access'])->nullable();
			});
		}

		public function down() {
			Schema::table('role', function ($table) {
				$table->dropColumn('special');
			});
		}

	}
