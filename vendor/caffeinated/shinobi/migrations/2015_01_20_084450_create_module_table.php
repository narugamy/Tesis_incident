<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;

	class CreateModuleTable extends Migration {

		public function up() {
			Schema::create('module', function (Blueprint $table) {
				$table->increments('id');
				$table->string('name')->unique();
				$table->string('slug')->unique();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::drop('module');
		}

	}
