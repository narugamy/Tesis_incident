<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;

	class CreateRoleTable extends Migration {

		public function up() {
			Schema::create('role', function (Blueprint $table) {
				$table->increments('id');
				$table->string('name')->unique();
				$table->string('slug')->unique();
				$table->text('description')->nullable();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::drop('role');
		}

	}
